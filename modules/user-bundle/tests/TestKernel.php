<?php

namespace App\Modules\UserBundle\Tests;

use App\Kernel;
use App\Modules\UserBundle\UserBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;

class TestKernel extends Kernel
{
    public function registerBundles(): array
    {
        return [
            new FrameworkBundle(),
            new DoctrineBundle(),

            new UserBundle()
        ];
    }
}
