<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class AuthRequestDto
{
    #[Assert\NotBlank(message: 'AUTH_EMPTY_PID_MESSAGE')]
    public string $username;

    #[Assert\NotBlank(message: 'AUTH_EMPTY_PASS_MESSAGE')]
    public string $password;
}
