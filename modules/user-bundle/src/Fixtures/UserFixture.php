<?php

namespace App\Modules\UserBundle\Fixtures;

use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

final class UserFixture extends Fixture
{
    public function __construct(private PasswordHasherInterface $hasher)
    {
    }

    public function load(ObjectManager $manager)
    {
        $manager->persist(new User('000000', 'admin@uta.com', 'Super Admin', new DateTimeImmutable(), $this->hasher->hash('123456'), ['ROLE_ADMIN', 'ROLE_USER', 'ROLE_PILOT']));
        $manager->flush();
    }
}
