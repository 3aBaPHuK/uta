<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Repository;

use App\Modules\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function existBy(string $key, string $value): bool
    {
        return (bool) $this->findOneBy([$key => $value]);
    }

    public function save(User $user) {
        $this->_em->persist($user);
        $this->_em->flush();
    }
}
