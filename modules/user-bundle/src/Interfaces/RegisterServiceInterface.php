<?php

namespace App\Modules\UserBundle\Interfaces;

use App\Modules\UserBundle\Dto\RegisterRequestDto;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

interface RegisterServiceInterface
{
    /**
     * @throws BadRequestHttpException | ConflictHttpException
     */
    public function register(RegisterRequestDto $request): void;
}
