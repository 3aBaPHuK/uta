<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutListener
{
    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $logoutEvent): void
    {
        $logoutEvent->getRequest()->getSession()->invalidate();

        $logoutEvent->setResponse(new JsonResponse());
    }
}
