<?php

namespace App\Modules\AiracUploaderBundle\Controller\Dto;

use App\Modules\AiracUploaderBundle\Entity\Airac;

final class AiracFileStatusDto
{
    private function __construct(
        public int $airacId,
        public string $fileName,
        public string $status,
        public ?string $cycle
    )
    {
    }

    public static final function fromEntity(Airac $airac): self
    {
        return new self($airac->getId(), $airac->getArchiveName(), $airac->getStatus(), $airac->getCycle());
    }
}
