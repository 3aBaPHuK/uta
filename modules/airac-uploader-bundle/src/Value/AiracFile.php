<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Value;

final class AiracFile
{
    public function __construct(private string $fileName, private string $status)
    {
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
