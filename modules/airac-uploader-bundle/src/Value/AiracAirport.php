<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Value;

final class AiracAirport
{
    public function __construct(
        private string $icao,
        private string $name,
        private int    $elevation,
        private int    $latitude,
        private int    $longitude
    )
    {
    }

    public function icao(): string
    {
        return $this->{__FUNCTION__};
    }


    public function name(): string
    {
        return $this->{__FUNCTION__};
    }

    public function elevation(): int
    {
        return $this->{__FUNCTION__};
    }

    public function latitude(): int
    {
        return $this->{__FUNCTION__};
    }

    public function longitude(): int
    {
        return $this->{__FUNCTION__};
    }
}
