<?php

namespace App\Modules\AiracUploaderBundle\Admin\Dashboard\Block;

use App\Modules\AiracUploaderBundle\Interfaces\AiracServiceInterface;
use App\Modules\AiracUploaderBundle\Repository\AiracRepository;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use function assert;

class AiracBlock extends AbstractBlockService
{
    private AiracServiceInterface $airacService;
    private AiracRepository $airacRepository;

    public function __construct(Environment $twig, AiracServiceInterface $airacService, AiracRepository $airacRepository)
    {
        $this->airacService = $airacService;
        $this->airacRepository = $airacRepository;

        parent::__construct($twig);
    }

    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'template' => '@AiracBundle/airac_dashboard_block.html.twig',
        ]);
    }

    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {
        $template = $blockContext->getTemplate();
        assert(null !== $template);

        return $this->renderResponse($template, [
            'block_context' => $blockContext,
            'block' => $blockContext->getBlock(),
            'currentAiracCycle' => $this->airacService->currentUploadedVersion(),
            'airac' => $this->airacRepository->findOneBy([], ['id' => 'DESC'])
        ], $response);
    }
}
