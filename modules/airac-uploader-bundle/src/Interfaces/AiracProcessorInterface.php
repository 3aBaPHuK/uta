<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Interfaces;

interface AiracProcessorInterface
{
    public function process(string $filesPath): void;

    public function supported(string $filesPath): bool;

    public function cycle(string $filesPath): string;
}
