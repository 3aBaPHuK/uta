<?php

namespace App\Modules\AiracUploaderBundle\Interfaces;

use App\Modules\AiracUploaderBundle\Entity\Airac;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface AiracServiceInterface
{
    public function currentUploadedVersion(): ?string;

    public function uploadNewAiracZip(UploadedFile $file): int;

    public function processAirac(int $airacId): void;

    public function getAirac(int $airacId): ?Airac;

    public function getLatestFileStatus(): ?Airac;
}
