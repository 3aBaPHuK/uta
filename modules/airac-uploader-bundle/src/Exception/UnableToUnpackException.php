<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Exception;

use RuntimeException;

class UnableToUnpackException extends RuntimeException
{
    public static function fromFileName(string $fileName): void
    {
        throw new self('Unable to unpack "'.$fileName.'"');
    }
}
