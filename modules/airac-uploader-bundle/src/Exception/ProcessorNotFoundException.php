<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Exception;

use RuntimeException;

class ProcessorNotFoundException extends RuntimeException
{
    public static function fromFileName(string $fileName): void
    {
        throw new self('Processor for "'.$fileName.'" not found');
    }
}
