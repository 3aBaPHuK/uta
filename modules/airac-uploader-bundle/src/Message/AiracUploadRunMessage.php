<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Message;

class AiracUploadRunMessage
{
    private int $airacId;

    public function __construct(int $airacId)
    {
        $this->airacId = $airacId;
    }

    public function getAiracId(): int
    {
        return $this->airacId;
    }
}
