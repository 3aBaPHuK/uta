<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Dto;

use App\Dto\AbstractPaginatedCollection;
use App\Modules\ConfigurationBundle\Entity\Configuration;
use App\Modules\ConfigurationBundle\Value\ConfigurationValuesCollection;

class ConfigurationValuesDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, ConfigurationValueDto ...$values)
    {
        $this->totalResults = $totalResults;
        $this->items = $values;
    }

    public function current(): ConfigurationValueDto
    {
        return current($this->items);
    }

    public static function fromValuesCollection(ConfigurationValuesCollection $valuesCollection): self
    {
        $valuesArrayCollection = array_map(function (Configuration $configurationValue) {
            return ConfigurationValueDto::fromEntity($configurationValue);
        }, iterator_to_array($valuesCollection));

        return new self(count($valuesCollection), ...$valuesArrayCollection);
    }
}
