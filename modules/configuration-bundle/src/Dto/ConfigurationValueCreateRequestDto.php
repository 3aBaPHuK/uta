<?php

namespace App\Modules\ConfigurationBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

final class ConfigurationValueCreateRequestDto
{
    public function __construct(
        public readonly string $label,
        public readonly string $value
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);

        return new self($data['label'], $data['value']);
    }
}
