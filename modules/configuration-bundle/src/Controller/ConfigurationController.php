<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Controller;

use App\Modules\ConfigurationBundle\Dto\ConfigurationValueCreateRequestDto;
use App\Modules\ConfigurationBundle\Dto\ConfigurationValueDto;
use App\Modules\ConfigurationBundle\Dto\ConfigurationValuesDto;
use App\Modules\ConfigurationBundle\Interfaces\ConfigurationInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfigurationController extends AbstractController
{
    #[Tag("Configuration")]
    #[Route(path: "api/admin/configuration", methods: ["GET"])]
    #[IsGranted("ROLE_ADMIN")]
    public function getActualConfigurationValues(ConfigurationInterface $configuration): JsonResponse
    {
        return new JsonResponse(ConfigurationValuesDto::fromValuesCollection($configuration->getValuesList()));
    }

    #[Tag("Configuration")]
    #[Route(path: "api/admin/configuration/{key}", methods: ["GET"])]
    #[IsGranted("ROLE_ADMIN")]
    public function getConfigurationValueByKey(ConfigurationInterface $configuration, string $key): JsonResponse
    {
        return new JsonResponse(ConfigurationValueDto::fromEntity($configuration->getValueByKey($key)));
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=ConfigurationValueCreateRequestDto::class)))
     */
    #[Tag("Configuration")]
    #[Route(path: "api/admin/configuration/{key}", methods: ["PUT", "POST"])]
    #[IsGranted("ROLE_ADMIN")]
    public function updateConfigurationValueByKey(ConfigurationInterface $configuration, string $key, Request $request): JsonResponse
    {
        $request = ConfigurationValueUpdateDto::fromRequest($request);
        $configuration->setValue($key, $request->label, $request->value);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
