<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Fixtures;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use App\Modules\ConfigurationBundle\Repository\ConfigurationRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class InitialConfigurationFixture extends Fixture
{
    public function __construct(private readonly ParameterBagInterface $parameterBag, private readonly ConfigurationRepository $configurationRepository)
    {
    }

    public function load(ObjectManager $manager)
    {
        $defaultAirlineConfiguration = new Configuration('default.airline_icao', 'Default Airline ICAO', $this->parameterBag->get('configuration.default_airline_icao'));
        $this->configurationRepository->save($defaultAirlineConfiguration);

        $defaultBaseAirportConfiguration = new Configuration('default.base_airport_icao', 'Default Base Airport', $this->parameterBag->get('configuration.default_base_airport_icao'));
        $this->configurationRepository->save($defaultBaseAirportConfiguration);
    }
}
