<?php

namespace App\Modules\ConfigurationBundle\Service;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use App\Modules\ConfigurationBundle\Interfaces\ConfigurationInterface;
use App\Modules\ConfigurationBundle\Repository\ConfigurationRepository;
use App\Modules\ConfigurationBundle\Value\ConfigurationValuesCollection;

class ConfigurationService implements ConfigurationInterface
{
    public function __construct(private readonly ConfigurationRepository $repository)
    {
    }

    public function getValueByKey(string $key): ?Configuration
    {
        return $this->getValuesList()->getByKey($key);
    }

    public function getValuesList(): ConfigurationValuesCollection
    {
        return $this->repository->all();
    }

    public function setValue(string $key, string $label, string $value): void
    {
        $this->repository->updateByKey($key, $label, $value);
    }
}
