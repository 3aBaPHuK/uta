<?php

namespace App\Modules\ConfigurationBundle\Interfaces;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use App\Modules\ConfigurationBundle\Value\ConfigurationValuesCollection;

interface ConfigurationInterface
{
    public function getValueByKey(string $key): ?Configuration;

    public function getValuesList(): ConfigurationValuesCollection;

    public function setValue(string $key, string $label, string $value): void;
}
