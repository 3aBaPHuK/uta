<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Entity;

use App\Modules\ConfigurationBundle\Repository\ConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Validator\Constraints as Assert;

#[Entity(repositoryClass: ConfigurationRepository::class)]
class Configuration
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 40)]
    #[Assert\Regex(pattern: '/^[a-z0-9]+$/i', message: 'The only letter or numbers is possible', htmlPattern: '^[a-zA-Z0-9]+$')]
    private string $key;

    #[ORM\Column(type: 'string')]
    private string $label;

    #[ORM\Column(type: 'string')]
    #[Assert\Regex(pattern: '/^[a-z0-9]+$/i', message: 'The only letter or numbers is possible', htmlPattern: '^[a-zA-Z0-9]+$')]
    private string $value;

    public function __construct(
        string $key,
        string $label,
        string $value
    )
    {
        $this->key = $key;
        $this->update($label, $value);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function update(string $label, ?string $value): self
    {
        $this->label = $label;
        $this->value = $value;

        return $this;
    }
}
