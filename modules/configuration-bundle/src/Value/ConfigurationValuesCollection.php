<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Value;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use Countable;
use IteratorAggregate;
use Traversable;

class ConfigurationValuesCollection implements IteratorAggregate, Countable
{
    private array $configurationValues = [];

    public function __construct(Configuration ...$configurationValues)
    {
        foreach ($configurationValues as $value) {
            $this->configurationValues[$value->getKey()] = $value;
        }
    }

    public function getByKey(string $key): ?Configuration
    {
        if (empty($this->configurationValues[$key]))
        {
            return null;
        }

        return $this->configurationValues[$key];
    }

    public function getIterator(): Traversable
    {
        foreach ($this->configurationValues as $value) {
            yield $value;
        }
    }

    public function count(): int
    {
        return count($this->configurationValues);
    }
}
