<?php

namespace App\Modules\TimetableBundle\Factory;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\AirportBundle\Repository\AirportRepository;
use App\Modules\TimetableBundle\Entity\Flight;
use DateTimeImmutable;
use InvalidArgumentException;

final class ParametersAwareFlightFactory
{
    public function __construct(
        private readonly AirportDetailsServiceInterface $airportDetailsService,
        private readonly AirportRepository $airportRepository,
        private readonly AircraftTypeRepository $aircraftTypeRepository,
        private readonly int $alternatesRange = 500
    )
    {
    }

    public function createFromParameters(
        string $flightNo,
        string $from,
        string $to,
        array $daysOfWeek,
        DateTimeImmutable $departureTime,
        DateTimeImmutable $since,
        DateTimeImmutable $till,
        string $acType
    ): Flight
    {
        $fromAirport = $this->airportRepository->findByIcao($from);
        $toAirport = $this->airportRepository->findByIcao($to);
        $aircraftType = $this->aircraftTypeRepository->findByIcao($acType);

        $this->assertParametersCorrect($fromAirport, $toAirport, $aircraftType);

        $alternatesArray = $this->airportDetailsService->getNearestByRange($this->alternatesRange, $toAirport);

        $flight = new Flight($flightNo, $fromAirport, $toAirport, $daysOfWeek, $departureTime, $since, $till, $aircraftType);

        foreach ($alternatesArray as $alternate) {
            $flight->addAlternate($alternate);
        }

        return $flight;
    }

    public function updateFromParameters(
        Flight $flight,
        string $flightNo,
        string $from,
        string $to,
        array $daysOfWeek,
        DateTimeImmutable $departureTime,
        DateTimeImmutable $since,
        DateTimeImmutable $till,
        string $acType
    ): Flight
    {
        $fromAirport = $this->airportRepository->findByIcao($from);
        $toAirport = $this->airportRepository->findByIcao($to);
        $aircraftType = $this->aircraftTypeRepository->findByIcao($acType);

        $this->assertParametersCorrect($fromAirport, $toAirport, $aircraftType);

        $flight->updateSchedule($since, $till, $departureTime, $daysOfWeek);
        $flight->updateRoute($fromAirport, $toAirport);
        $flight->setFlightNumber($flightNo);
        $flight->setAircraftType($aircraftType);

        return $flight;
    }

    public function assertParametersCorrect(?Airport $fromAirport, ?Airport $toAirport, ?AircraftType $aircraftType): void
    {
        if (!$fromAirport || !$toAirport || !$aircraftType) {
            throw new InvalidArgumentException("Incorrect creation params.");
        }
    }
}
