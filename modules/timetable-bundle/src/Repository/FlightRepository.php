<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Repository;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TimetableBundle\Factory\ParametersAwareFlightFactory;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FlightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private readonly ParametersAwareFlightFactory $factory)
    {
        parent::__construct($registry, Flight::class);
    }

    public function save(Flight $flight): void
    {
        $this->_em->persist($flight);
        $this->_em->flush();
    }

    public function findByFlightNumber(string $flightNumber): ?Flight
    {
        return $this->findOneBy(['flightNumber' => $flightNumber]);
    }

    public function createOrUpdateByFlightNumber(
        string $flightNo,
        string $fromAirport,
        string $toAirport,
        array $daysOfWeek,
        DateTimeImmutable $departureTime,
        DateTimeImmutable $since,
        DateTimeImmutable $till,
        string $aircraftType
    ): Flight
    {
        if (!$flight = $this->findByFlightNumber($flightNo)) {
            $flight = $this->factory->createFromParameters($flightNo, $fromAirport, $toAirport, $daysOfWeek, $departureTime, $since, $till, $aircraftType);
        } else {
            $flight = $this->factory->updateFromParameters($flight, $flightNo, $fromAirport, $toAirport, $daysOfWeek, $departureTime, $since, $till, $aircraftType);
        }

        $this->save($flight);

        return $flight;
    }

    public function createFromDto(TimetableItemDto $dto): void
    {
        $flight = $this->createOrUpdateByFlightNumber(
            $dto->flightNumber,
            $dto->from,
            $dto->to,
            $dto->daysOfWeek,
            new DateTimeImmutable($dto->etd),
            new DateTimeImmutable($dto->scheduledSince),
            new DateTimeImmutable($dto->scheduledTill),
            $dto->aircraftType
        );

        $this->save($flight);
    }

    public function deleteRecordById(int $id): void
    {
        $flightRef = $this->_em->getReference(Flight::class, $id);

        $this->_em->remove($flightRef);
        $this->_em->flush();
    }

    public function findByParameters(
        int $page,
        int $pageSize,
        ?DateTimeImmutable $actualDate,
        ?string $from = null,
        ?string $to = null,
        ?string $aircraftType = null,
    ): TimetableCollectionDto
    {
        $dayOfWeek = (int) $actualDate->format('w');
        $dayOfWeek = !$dayOfWeek ? 7 : $dayOfWeek;

        $qb = $this->createQueryBuilder('f');

        $qb->leftJoin('f.from', 'af');
        $qb->leftJoin('f.to', 'at');
        $qb->leftJoin('f.aircraftType', 'aircraftType');

        if ($from) {
            $qb->andWhere('af.icao = :from');
            $qb->setParameter('from', $from);
        }

        if ($to) {
            $qb->andWhere('at.icao = :to');
            $qb->setParameter('to', $to);
        }

        if ($aircraftType) {
            $qb->andWhere('aircraftType.icao = :aircraftType');
            $qb->setParameter('aircraftType', $aircraftType);
        }

        $qb->andWhere('f.scheduledSince < :date AND f.scheduledTill >= :date');
        $qb->andWhere($qb->expr()->like('f.daysOfWeek', ':dayOfWeek'));
        $qb->setParameter('date', $actualDate->format('Y-m-d'));
        $qb->setParameter('dayOfWeek', '%' . $dayOfWeek . '%');
        $qb->setFirstResult(($page - 1) * $pageSize);
        $qb->setMaxResults($pageSize);

        $res = $qb->getQuery()->getResult();

        return new TimetableCollectionDto($this->count([]), ...array_map(function (Flight $flight) {
            return TimetableItemDto::fromFlightEntity($flight);
        }, $res));
    }
}
