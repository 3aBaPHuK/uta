<?php

namespace App\Modules\TimetableBundle\Interfaces;

use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use App\Modules\TimetableBundle\Entity\Flight;

interface TimetableCrudServiceInterface
{
    public function createFlightFromDto(TimetableItemDto $dto): Flight;

    public function updateFlightFromDto(int $id, TimetableItemDto $dto): Flight;

    public function deleteFlight(int $id): void;
}
