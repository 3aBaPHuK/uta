<?php

namespace App\Modules\TimetableBundle\Interfaces;

use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use DateTimeImmutable;

interface TimetableServiceInterface
{
    public function getActualSchedule(
        DateTimeImmutable $actualDate,
        ?string $from,
        ?string $to,
        ?string $aircraftType,
        int $page,
        int $pageSize
    ): TimetableCollectionDto;
}
