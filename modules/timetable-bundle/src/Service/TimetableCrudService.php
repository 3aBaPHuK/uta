<?php

namespace App\Modules\TimetableBundle\Service;

use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TimetableBundle\Factory\ParametersAwareFlightFactory;
use App\Modules\TimetableBundle\Interfaces\TimetableCrudServiceInterface;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use DateTimeImmutable;

class TimetableCrudService implements TimetableCrudServiceInterface
{
    public function __construct(
        private readonly FlightRepository $flightRepository,
        private readonly ParametersAwareFlightFactory $factory
    )
    {
    }

    public function createFlightFromDto(TimetableItemDto $dto): Flight
    {
        $flight = $this->factory->createFromParameters(
            $dto->flightNumber,
            $dto->from,
            $dto->to,
            $dto->daysOfWeek,
            new DateTimeImmutable($dto->etd),
            new DateTimeImmutable($dto->scheduledSince),
            new DateTimeImmutable($dto->scheduledTill),
            $dto->aircraftType
        );

        $this->flightRepository->save($flight);

        return $flight;
    }

    public function updateFlightFromDto(int $id, TimetableItemDto $dto): Flight
    {
        $flight = $this->factory->updateFromParameters(
            $this->flightRepository->find($id),
            $dto->flightNumber,
            $dto->from,
            $dto->to,
            $dto->daysOfWeek,
            new DateTimeImmutable($dto->etd),
            new DateTimeImmutable($dto->scheduledSince),
            new DateTimeImmutable($dto->scheduledTill),
            $dto->aircraftType
        );

        $this->flightRepository->save($flight);

        return $flight;
    }

    public function deleteFlight(int $id): void
    {
        $this->flightRepository->deleteRecordById($id);
    }
}
