<?php

namespace App\Modules\TimetableBundle\Service;

use App\Modules\TimetableBundle\Interfaces\TimetableServiceInterface;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use DateTimeImmutable;

class TimetableService implements TimetableServiceInterface
{
    public function __construct(private readonly FlightRepository $flightRepository)
    {
    }

    public function getActualSchedule(
        DateTimeImmutable $actualDate,
        ?string $from = null,
        ?string $to = null,
        ?string $aircraftType = null,
        int $page = 1,
        int $pageSize = 15
    ): TimetableCollectionDto
    {
        return $this->flightRepository->findByParameters($page, $pageSize, $actualDate, $from, $to, $aircraftType);
    }
}
