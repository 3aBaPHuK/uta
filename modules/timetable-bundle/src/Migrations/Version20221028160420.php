<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221028160420 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added flight number to flight entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE flight ADD flight_number VARCHAR(7) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE flight DROP flight_number');
    }
}
