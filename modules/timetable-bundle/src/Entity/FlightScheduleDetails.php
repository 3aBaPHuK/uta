<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Entity;

use DateTimeImmutable;

final class FlightScheduleDetails
{
    public function __construct(
        private readonly DateTimeImmutable $scheduledSince,
        private readonly DateTimeImmutable $scheduledTill,
        private readonly DateTimeImmutable $departureTime,
        private readonly array             $daysOfWeek
    )
    {
    }

    public function scheduledSince(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function scheduledTill(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function departureTime(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function daysOfWeek(): array
    {
        return $this->{__FUNCTION__};
    }
}
