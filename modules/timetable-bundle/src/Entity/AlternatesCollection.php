<?php

namespace App\Modules\TimetableBundle\Entity;

use App\Modules\AirportBundle\Entity\Airport;
use Generator;
use IteratorAggregate;

class AlternatesCollection implements IteratorAggregate
{
    private array $alternates;

    public function __construct(Airport ...$alternates)
    {
        foreach ($alternates as $alternate) {
            $this->alternates[$alternate->getIcao()] = $alternate;
        }
    }

    public function getIterator(): Generator
    {
        foreach ($this->alternates as $alternate) {
            yield $alternate;
        }
    }
}
