<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class TimetableCollectionDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, TimetableItemDto ...$pilots)
    {
        $this->items = $pilots;
        $this->totalResults = $totalResults;
    }
    public function current(): TimetableItemDto
    {
        return current($this->items);
    }
}
