<?php

namespace App\Modules\TimetableBundle\Controller;

use App\Modules\TimetableBundle\Interfaces\TimetableServiceInterface;
use DateTimeImmutable;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Modules\TimetableBundle\Dto\TimetableRequestDto;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;

class TimetableController extends AbstractController
{
    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=TimetableRequestDto::class)))
     */
    #[Route(path: '/api/flight/timetable', name: 'timetable', methods: ['POST'])]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[Tag('Flight')]
    #[IsGranted('ROLE_PILOT')]
    public function getTimetable(Request $request, TimetableServiceInterface $timetableService): JsonResponse
    {
        $timetableDto = TimetableRequestDto::fromRequest($request);

        return new JsonResponse($timetableService->getActualSchedule(
            $timetableDto->getDateOfFlight() ?? new DateTimeImmutable('today'),
            $timetableDto->getFrom(),
            $timetableDto->getTo(),
            $timetableDto->getAircraftType(),
            (int)$request->get('page', 1),
            (int)$request->get('page_size', 15)
        ));
    }
}
