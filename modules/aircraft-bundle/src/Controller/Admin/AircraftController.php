<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Controller\Admin;

use App\Modules\AircraftBundle\Dto\AircraftDto;
use App\Modules\AircraftBundle\Interface\AircraftCrudServiceInterface;
use App\Modules\AircraftBundle\Repository\AircraftRepository;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation as NA;

class AircraftController extends AbstractController
{
    public function __construct(private AircraftCrudServiceInterface $aircraftCrudService, private AircraftRepository $aircraftRepository)
    {
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AircraftDto::class))) */
    #[Route(path: '/api/admin/aircraft', methods: ['POST'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function createAircraft(Request $request): JsonResponse
    {
        $dto = AircraftDto::fromRequest($request);

        $this->aircraftCrudService->addAircraft($dto->icao, $dto->location, $dto->gate, $dto->registration, $dto->fob);

        return new JsonResponse($dto, Response::HTTP_CREATED);
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AircraftDto::class))) */
    #[Route(path: '/api/admin/aircraft/{id}', methods: ['PUT'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAircraft(int $id, Request $request): JsonResponse
    {
        $dto = AircraftDto::fromRequest($request);

        $this->aircraftCrudService->updateAircraft($id, $dto->icao, $dto->location, $dto->gate, $dto->registration, $dto->fob);

        return new JsonResponse($dto);
    }

    #[Route(path: '/api/admin/aircraft/{id}', methods: ['GET'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function getAircraft(int $id): JsonResponse
    {
        return new JsonResponse(AircraftDto::fromEntity($this->aircraftRepository->find($id)));
    }

    #[Route(path: '/api/admin/aircraft/{id}', methods: ['DELETE'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function removeAircraft(int $id): JsonResponse
    {
        $this->aircraftCrudService->deleteAircraft($id);

        return new JsonResponse();
    }

    #[Route(path: '/api/admin/aircrafts', methods: ['GET'])]
    #[Tag('Aircraft')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[IsGranted('ROLE_ADMIN')]
    public function list(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->aircraftCrudService->getAircraftList(
                (int) $request->get('page', 1),
                (int) $request->get('page_size', 15)
            )
        );
    }
}
