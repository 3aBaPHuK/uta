<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\EventSubscriber;

use App\Modules\AircraftBundle\Repository\AircraftRepository;
use App\Modules\TripBundle\Event\TripFinishedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TripFinishedEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly AircraftRepository $aircraftRepository)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [TripFinishedEvent::class => 'onTripFinished'];
    }

    public function onTripFinished(TripFinishedEvent $event): void
    {
        if (!$aircraft = $event->assignedAircraft())
        {
            return;
        }

        $tripDepartureTime = $event->trip()->getFirstLeg()->departureDateTime();
        $tripArrivalTime = $event->trip()->getLastLeg()->arrived();

        $aircraft->getAircraftStats()->updateStats($tripDepartureTime, $tripArrivalTime);
        $this->aircraftRepository->save($aircraft);
    }
}
