<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Fixtures;

use App\Modules\AircraftBundle\Entity\AircraftType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class AircraftTypeFixture extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $manager->persist(new AircraftType('B738', 'Boeing 737-800', 'Boeing 737-800', '', false));
        $manager->persist(new AircraftType('B737', 'Boeing 737-700', 'Boeing 737-700', '', false));
        $manager->persist(new AircraftType('B735', 'Boeing 737-500', 'Boeing 737-500', '', false));
        $manager->persist(new AircraftType('B734', 'Boeing 737-400', 'Boeing 737-400', '', false));
        $manager->persist(new AircraftType('B762', 'Boeing 767-200', 'Boeing 767-200', '', false));
        $manager->persist(new AircraftType('B763', 'Boeing 767-300', 'Boeing 767-300', '', false));
        $manager->persist(new AircraftType('AT75', 'ATR 72-500', 'ATR 72-500', '', false));
        $manager->persist(new AircraftType('T154', 'Tupolev TU-154', 'Tupolev TU-154', '', false));
        $manager->persist(new AircraftType('T134', 'Tupolev TU-134', 'Tupolev TU-134', '', false));
        $manager->persist(new AircraftType('AN24', 'Antonov AN-24', 'Antonov AN-24', '', false));

        $manager->flush();
    }
}
