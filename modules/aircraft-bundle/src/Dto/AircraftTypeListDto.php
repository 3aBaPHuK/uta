<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class AircraftTypeListDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, AircraftTypeDto ...$aircraftTypes)
    {
        $this->items = $aircraftTypes;
        $this->totalResults = $totalResults;
    }

    public function current(): AircraftTypeDto
    {
        return current($this->items);
    }
}
