<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Dto;

use App\Modules\AircraftBundle\Entity\Aircraft;
use Symfony\Component\HttpFoundation\Request;

final class AircraftDto
{
    private function __construct(
        public ?int $id,
        public string $icao,
        public string $location,
        public string $gate,
        public string $registration,
        public int $fob,
        public readonly ?AircraftStatsDto $stats = null
    )
    {
    }

    public static final function fromRequest(Request $request): self
    {
        $requestBody = json_decode($request->getContent(), true);

        return new self(
            null,
            $requestBody['icao'],
            $requestBody['location'],
            $requestBody['gate'],
            $requestBody['registration'],
            (int) $requestBody['fob'],
        );
    }

    public static final function fromEntity(Aircraft $aircraft): self
    {
        $me = new self(
            $aircraft->getId(),
            $aircraft->getType()->getIcao(),
            $aircraft->getLocation()->getIcao(),
            $aircraft->getGate(),
            $aircraft->getRegistration(),
            $aircraft->getFob(),
            AircraftStatsDto::fromEntity($aircraft->getAircraftStats())
        );

        $me->id = $aircraft->getId();

        return $me;
    }
}
