<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class AircraftListDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, AircraftDto ...$aircrafts)
    {
        $this->items = $aircrafts;
        $this->totalResults = $totalResults;
    }

    public function current(): AircraftDto
    {
        return current($this->items);
    }
}
