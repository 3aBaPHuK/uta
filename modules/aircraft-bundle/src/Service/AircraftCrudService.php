<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Service;

use App\Modules\AircraftBundle\Dto\AircraftDto;
use App\Modules\AircraftBundle\Dto\AircraftListDto;
use App\Modules\AircraftBundle\Dto\AircraftTypeDto;
use App\Modules\AircraftBundle\Dto\AircraftTypeListDto;
use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AircraftBundle\Interface\AircraftCrudServiceInterface;
use App\Modules\AircraftBundle\Interface\AircraftTypeCrudServiceInterface;
use App\Modules\AircraftBundle\Repository\AircraftRepository;
use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AircraftCrudService implements AircraftCrudServiceInterface, AircraftTypeCrudServiceInterface
{
    public function __construct(
        private readonly AircraftRepository $aircraftRepository,
        private readonly AircraftTypeRepository $aircraftTypeRepository,
        private readonly AirportDetailsServiceInterface $airportDetailsService,
        private readonly string $imagesPath,
        private readonly string $publicImagesPath
    )
    {
    }

    public function addAircraft(string $icao, string $location, string $gate, string $registration, int $fob): void
    {
        $aircraftType = $this->aircraftTypeRepository->findByIcao($icao);
        $location = $this->airportDetailsService->getAirportByIcao($location);

        if (!$aircraftType) {
            throw new BadRequestHttpException("Unknown aircraft type: '$icao'");
        }

        if (!$location) {
            throw new BadRequestHttpException("Unknown location: '$location'");
        }

        $this->aircraftRepository->save(new Aircraft($aircraftType, $registration, $location, $gate, $fob));
    }

    public function updateAircraft(int $id, string $icao, string $location, string $gate, string $registration, int $fob): void
    {
        $aircraftType = $this->aircraftTypeRepository->findByIcao($icao);
        $location = $this->airportDetailsService->getAirportByIcao($location);

        $aircraft = $this->aircraftRepository->getById($id);

        $aircraft->moveToLocation($location, $gate, $fob);
        $aircraft->setType($aircraftType);
        $aircraft->updateRegistration($registration);

        $this->aircraftRepository->save($aircraft);
    }

    public function deleteAircraft(int $id): void
    {
        $this->aircraftRepository->deleteById($id);
    }

    public function addAircraftType(string $icao, string $name, string $description, string $image, bool $heavy): void
    {
        $this->aircraftTypeRepository->save(new AircraftType($icao, $name, $description, $image, $heavy));
    }

    public function updateAircraftType(int $id, string $icao, string $name, string $description, string $image, bool $heavy): void
    {
        $aircraftType = $this->aircraftTypeRepository->getById($id);

        $aircraftType->setIcao($icao);
        $aircraftType->setName($name);
        $aircraftType->setDescription($description);
        $aircraftType->setImage($image);
        $aircraftType->setHeavy($heavy);

        $this->aircraftTypeRepository->save($aircraftType);
    }

    public function deleteAircraftType(int $id): void
    {
        $this->aircraftTypeRepository->deleteById($id);
    }

    public function getAircraftList(int $page, int $pageSize): AircraftListDto
    {
        $aircraft = $this->aircraftRepository->findBy([], [], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->aircraftRepository->count([]);

        $aircraftList = [];

        foreach ($aircraft as $airport) {
            $aircraftList[] = AircraftDto::fromEntity($airport);
        }

        return new AircraftListDto($totalCount, ...$aircraftList);
    }

    public function getAircraftTypesList(int $page, int $pageSize): AircraftTypeListDto
    {
        $aircraft = $this->aircraftTypeRepository->findBy([], ['id' => 'DESC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->aircraftTypeRepository->count([]);

        $aircraftTypesList = [];

        foreach ($aircraft as $airport) {
            $aircraftTypesList[] = AircraftTypeDto::fromEntity($airport);
        }

        return new AircraftTypeListDto($totalCount, ...$aircraftTypesList);
    }

    public function uploadAircraftTypeImage(UploadedFile $file): string
    {
        $file->move($this->imagesPath, $file->getClientOriginalName());

        return $this->publicImagesPath.DIRECTORY_SEPARATOR.$file->getClientOriginalName();
    }
}
