<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Entity;

use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity]
class AircraftStats
{
    const FIFTY_CHECK = 0;

    const TWO_HUNDRED_CHECK = 1;

    const ONE_THOUSAND_CHECK = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $lastFlightAt;

    #[ORM\Column(type: 'integer')]
    private int $acTotalFHMinutes;

    #[ORM\Column(type: 'integer')]
    private int $acTotalFC;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $latestFiftyCheckAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $latestTwoHundredCheckAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $latestThousandCheckAt;

    public function __construct(
        int $acTotalFHMinutes = 0,
        int $acTotalFC = 0,
        ?DateTimeImmutable $lastFlightAt = null,
        ?DateTimeImmutable $latestFiftyCheckAt = new DateTimeImmutable('now'),
        ?DateTimeImmutable $latestTwoHundredCheckAt = new DateTimeImmutable('now'),
        ?DateTimeImmutable $latestThousandCheckAt = new DateTimeImmutable('now'),
    )
    {
        $this->acTotalFHMinutes = $acTotalFHMinutes;
        $this->acTotalFC = $acTotalFC;
        $this->lastFlightAt = $lastFlightAt;
        $this->latestFiftyCheckAt = $latestFiftyCheckAt;
        $this->latestTwoHundredCheckAt = $latestTwoHundredCheckAt;
        $this->latestThousandCheckAt = $latestThousandCheckAt;
    }

    public function updateStats(DateTimeImmutable $departureTime, DateTimeImmutable $arrivalTime): void
    {
        $this->acTotalFHMinutes += max(0, $arrivalTime->diff($departureTime, true)->i);
        $this->acTotalFC++;
        $this->lastFlightAt = new DateTimeImmutable('now');
    }

    public function performCheck(int $type): void
    {
        switch ($type) {
            case self::FIFTY_CHECK:
                $this->latestFiftyCheckAt = new DateTimeImmutable();
                break;
            case self::TWO_HUNDRED_CHECK:
                $this->latestTwoHundredCheckAt = new DateTimeImmutable();
                break;
            case self::ONE_THOUSAND_CHECK:
                $this->latestThousandCheckAt = new DateTimeImmutable();
                break;
            default:
                throw new InvalidArgumentException("Unknown check type $type");
        }
    }

    public function timeToCheck(int $type): DateInterval
    {
        return match ($type) {
            self::FIFTY_CHECK => $this->lastFlightAt ? $this->lastFlightAt->diff($this->latestFiftyCheckAt) : new DateInterval('PT50H'),
            self::TWO_HUNDRED_CHECK => $this->lastFlightAt ? $this->lastFlightAt->diff($this->latestTwoHundredCheckAt) : new DateInterval('PT200H'),
            self::ONE_THOUSAND_CHECK => $this->lastFlightAt ? $this->lastFlightAt->diff($this->latestThousandCheckAt) : new DateInterval('PT1000H'),
            default => throw new InvalidArgumentException("Unknown check type $type"),
        };
    }

    public function getLastFlightAt(): ?DateTimeImmutable
    {
        return $this->lastFlightAt;
    }

    public function getFlightHours(): string
    {
        return date('H:i', mktime(0, $this->acTotalFHMinutes));
    }

    public function getAcTotalFC(): int
    {
        return $this->acTotalFC;
    }
}
