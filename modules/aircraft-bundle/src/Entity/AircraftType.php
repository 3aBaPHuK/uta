<?php

namespace App\Modules\AircraftBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AircraftType
{
    const SERVER_PATH_TO_IMAGE_FOLDER = '../public/uploads/images';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 4)]
    private string $icao;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'text')]
    private string $description;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $image;

    #[ORM\Column(type: 'boolean')]
    private bool $heavy;

    private ?File $imageFile;

    public function __construct(string $icao, string $name, string $description, string $image, bool $heavy)
    {
        $this->icao = $icao;
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->heavy = $heavy;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIcao(): string
    {
        return $this->icao;
    }

    public function setIcao(string $icao): void
    {
        $this->icao = $icao;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function isHeavy(): bool
    {
        return $this->heavy;
    }

    public function setHeavy(bool $heavy): void
    {
        $this->heavy = $heavy;
    }

    public function getImageFile(): File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $imageFile->move(
                realpath(self::SERVER_PATH_TO_IMAGE_FOLDER),
                $imageFile->getClientOriginalName()
            );

            $this->image = $imageFile->getClientOriginalName();
        }

        $this->imageFile = null;
    }
}
