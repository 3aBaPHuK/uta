<?php

namespace App\Modules\AircraftBundle\Interface;

use App\Modules\AircraftBundle\Dto\AircraftListDto;

interface AircraftCrudServiceInterface
{
    public function addAircraft(string $icao, string $location, string $gate, string $registration, int $fob): void;

    public function updateAircraft(int $id, string $icao, string $location, string $gate, string $registration, int $fob): void;

    public function deleteAircraft(int $id): void;

    public function getAircraftList(int $page, int $pageSize): AircraftListDto;
}
