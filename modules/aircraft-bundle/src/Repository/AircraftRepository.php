<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Repository;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AircraftBundle\Value\AircraftCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use OutOfBoundsException;

class AircraftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aircraft::class);
    }

    public function getById(int $id): Aircraft
    {
        $aircraft = $this->find($id);

        return $aircraft === null ? throw new OutOfBoundsException("Aircraft not found") : $aircraft;
    }

    public function findByAircraftType(AircraftType $aircraftType): AircraftCollection
    {
        return new AircraftCollection(...$this->findBy(['type' => $aircraftType]));
    }

    public function deleteById(int $id): void
    {
        $this->_em->remove($this->find($id));
        $this->_em->flush();
    }

    public function save(Aircraft $aircraft): void
    {
        $this->_em->persist($aircraft);
        $this->_em->flush();
    }
}
