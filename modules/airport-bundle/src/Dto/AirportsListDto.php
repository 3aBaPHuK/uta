<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

final class AirportsListDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, AirportDto ...$airports)
    {
        $this->items = $airports;
        $this->totalResults = $totalResults;
    }

    public function current(): AirportDto
    {
        return current($this->items);
    }
}
