<?php

namespace App\Modules\AirportBundle\Interfaces;

use App\Modules\AirportBundle\Entity\Airport;

interface BaseAirportServiceInterface
{
    const BASE_AIRPORT_SETTING_KEY = 'default.base_airport_icao';

    public function getBaseAirport(): Airport;
}
