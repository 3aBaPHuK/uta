<?php

namespace App\Modules\AirportBundle\Interfaces;

use App\Modules\AirportBundle\Dto\AirportsListDto;

interface AirportListServiceInterface
{
    public function getAirportList(int $page, int $pageSize): AirportsListDto;
}
