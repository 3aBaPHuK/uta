<?php

namespace App\Modules\AirportBundle\Interfaces;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\AirportBundle\Entity\Airport;

interface AirportDetailsServiceInterface
{
    public function getAirportByIcao(string $icao): ?Airport;

    public function getNearestByRange(int $rangeKilometers, Airport $airport): array;

    public function updateFromRequest(Airport $airport, AirportDto $airportDto): Airport;

    public function createFromRequest(AirportDto $airportDto): Airport;
}
