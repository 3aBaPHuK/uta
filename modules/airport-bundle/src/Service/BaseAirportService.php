<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\Service;

use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\AirportBundle\Interfaces\BaseAirportServiceInterface;
use App\Modules\AirportBundle\Repository\AirportRepository;
use App\Modules\ConfigurationBundle\Interfaces\ConfigurationInterface;

class BaseAirportService implements BaseAirportServiceInterface
{
    public function __construct(
        private readonly ConfigurationInterface $configuration,
        private readonly AirportDetailsServiceInterface $airportDetailsService,
        private readonly AirportRepository $airportRepository,
    )
    {
    }

    public function getBaseAirport(): Airport
    {
        $baseAirportIcao = $this->configuration->getValueByKey(self::BASE_AIRPORT_SETTING_KEY)?->getValue();

        if (!$airport = $this->airportDetailsService->getAirportByIcao($baseAirportIcao)) {
            $airport = new Airport($baseAirportIcao, '', '', null, null, 0, '', '0');
            $this->airportRepository->save($airport);
        }

        return $airport;
    }
}
