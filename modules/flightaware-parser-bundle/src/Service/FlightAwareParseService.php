<?php

namespace App\Modules\FlightawareParserBundle\Service;

use App\Modules\FlightawareParserBundle\Interfaces\FlightAwareParseFlightsInterface;
use App\Modules\FlightawareParserBundle\Transport\FlightawareClient;
use App\Modules\FlightawareParserBundle\Value\FlightAwareFlight;
use App\Modules\FlightawareParserBundle\Value\FlightawareFlightsCollection;
use RuntimeException;

class FlightAwareParseService implements FlightAwareParseFlightsInterface
{
    public function __construct(private readonly FlightawareClient $client)
    {
    }

    public function parseFlights(string $airlineIcao, ?int $maxPages = 10, ?bool $fromSnapshot = false): FlightawareFlightsCollection
    {
        $data = $fromSnapshot ?
            file_get_contents(__DIR__."/../docs/$airlineIcao.snapshot.json") :
            $this->client->get("/operators/$airlineIcao/flights?max_pages=$maxPages");

        if (!$data = json_decode($data, true)) {
            throw new RuntimeException("Something wrong");
        }

        $collection = new FlightawareFlightsCollection();

        $allFlightsData = array_merge($data['scheduled'], $data['arrivals'], $data['enroute']);

        foreach ($allFlightsData as $flightData) {
            $collection->add(new FlightAwareFlight(
                $flightData['ident_icao'],
                $flightData['origin']['code'],
                $flightData['destination']['code'],
                $flightData['scheduled_off'],
                $flightData['aircraft_type']
            ));
        }

        return $collection;
    }
}
