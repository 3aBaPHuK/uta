<?php

declare(strict_types=1);

namespace App\Modules\FlightawareParserBundle\Value;

final class FlightAwareFlight
{
    public function __construct(
        public readonly string $identIcao,
        public readonly string $origin,
        public readonly string $destination,
        public readonly string $scheduledOff,
        public readonly ?string $aircraftType = null
    )
    {
    }
}
