<?php

declare(strict_types=1);

namespace App\Modules\FlightawareParserBundle\Value;

use Iterator;
use Traversable;

final class FlightawareFlightsCollection implements Iterator
{
    private array $flights;

    public function __construct(FlightAwareFlight...$flights)
    {
        $this->flights = $flights;
    }

    public function add(FlightAwareFlight $flight): void
    {
        $this->flights[] = $flight;
    }

    public function getIterator(): Traversable
    {
        foreach ($this->flights as $flight) {
            yield $flight;
        }
    }

    public function current(): FlightAwareFlight
    {
        return current($this->flights);
    }

    public function next(): void
    {
        next($this->flights);
    }

    public function key(): int
    {
        return key($this->flights);
    }

    public function valid(): bool
    {
        return false !== current($this->flights);
    }

    public function rewind(): void
    {
        reset($this->flights);
    }
}
