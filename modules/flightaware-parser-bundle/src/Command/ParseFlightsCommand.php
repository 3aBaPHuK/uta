<?php

declare(strict_types=1);

namespace App\Modules\FlightawareParserBundle\Command;

use App\Modules\FlightawareParserBundle\Event\FlightawareScheduleImportFinishedEvent;
use App\Modules\FlightawareParserBundle\Interfaces\DomScheduleServiceInterface;
use App\Modules\FlightawareParserBundle\Interfaces\FlightAwareParseFlightsInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ParseFlightsCommand extends Command
{

    public function __construct(
        private readonly FlightAwareParseFlightsInterface $scheduleService,
        private readonly EventDispatcherInterface $eventDispatcher
    )
    {
        parent::__construct('flightaware:import-airports-and-flights');
    }

    protected function configure()
    {
        $this->setDescription('Import InfoGate schedule for company');
        $this->setHelp('Example: bin/console flightaware:import-airports-and-flights --icao="SBI" --snapshot=1');

        $this->addOption(
            'icao',
            null,
            InputOption::VALUE_REQUIRED,
            'for example: SBI'
        );

        $this->addOption(
            'snapshot',
            null,
            InputOption::VALUE_OPTIONAL,
            'for example: 1',
            0,
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $scheduleItemsCollection = $this->scheduleService->parseFlights(strtoupper($input->getOption('icao')), null, (bool) $input->getOption('snapshot'));

        $this->eventDispatcher->dispatch(new FlightawareScheduleImportFinishedEvent($scheduleItemsCollection));

        return 0;
    }
}
