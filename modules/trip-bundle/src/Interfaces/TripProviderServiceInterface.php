<?php

namespace App\Modules\TripBundle\Interfaces;

use App\Modules\AircraftBundle\Entity\AircraftType;
use DateInterval;
use DateTimeImmutable;

interface TripProviderServiceInterface
{
    public function availableTripListFor(
        AircraftType $type,
        DateTimeImmutable $date,
        DateInterval $flightDutyHours
    );
}
