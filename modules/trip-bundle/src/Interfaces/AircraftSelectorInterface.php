<?php

namespace App\Modules\TripBundle\Interfaces;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AirportBundle\Entity\Airport;

interface AircraftSelectorInterface
{
    public function getAvailableAircraftByTypeAndLocation(string $aircraftTypeIcao, Airport $location): ?Aircraft;
}
