<?php

namespace App\Modules\TripBundle\Interfaces;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Value\TripRequest;

interface TripActionInterface
{
    public function __invoke(TripRequest $tripRequest): Trip;
}
