<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\TripRequest;

class StartTripAction implements TripActionInterface
{
    public function __invoke(TripRequest $tripRequest): Trip
    {
        $tripRequest->trip()->startTrip();
        $tripRequest->repository()->save($tripRequest->trip());

        return $tripRequest->trip();
    }
}
