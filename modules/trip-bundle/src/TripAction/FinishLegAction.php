<?php

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\FinishLegRequest;
use App\Modules\TripBundle\Value\TripRequest;
use InvalidArgumentException;

class FinishLegAction implements TripActionInterface
{

    /** @param FinishLegRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        $trip = $tripRequest->trip();
        $legData = $tripRequest->lastLegData();

        if ($trip->currentLeg() !== $trip->getLastLeg())
        {
            throw new InvalidArgumentException("Current leg isn't last");
        }

        $trip->finishCurrentLeg($legData->arrivalGate, $legData->touchdownTime, $legData->arrivalTime, $legData->fuelOnBoard, $legData->touchdownFpm);
        $tripRequest->repository()->save($trip);

        return $trip;
    }
}
