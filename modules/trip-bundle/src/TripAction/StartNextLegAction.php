<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\StartNextLegRequest;
use App\Modules\TripBundle\Value\TripRequest;
use InvalidArgumentException;

class StartNextLegAction implements TripActionInterface
{

    /** @param StartNextLegRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        if (!$tripRequest->trip()->nextLeg()) {
            throw new InvalidArgumentException("Next leg not available");
        }

        $previousLegData = $tripRequest->lastLegData();
        $tripRequest->trip()->finishCurrentLeg($previousLegData->arrivalGate, $previousLegData->touchdownTime, $previousLegData->arrivalTime, $previousLegData->fuelOnBoard, $previousLegData->touchdownFpm);
        $tripRequest->trip()->startNextLeg();
        $tripRequest->repository()->save($tripRequest->trip());

        return $tripRequest->trip();
    }
}
