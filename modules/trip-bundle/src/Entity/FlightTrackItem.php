<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class FlightTrackItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: FlightTrack::class)]
    private FlightTrack $track;

    public function __construct(
        #[ORM\Column(type: 'decimal', precision: 20, scale: 16, nullable: true)]
        private readonly ?string $latitude,
        #[ORM\Column(type: 'decimal', precision: 20, scale: 16, nullable: true)]
        private readonly ?string $longitude,
        #[ORM\Column(type: 'integer')]
        private readonly int $bank,
        #[ORM\Column(type: 'integer')]
        private readonly int $pitch,
        #[ORM\Column(type: 'boolean')]
        private readonly bool $isLandingLightsOn,
        #[ORM\Column(type: 'smallint')]
        private readonly int $parkingBrake
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getBank(): int
    {
        return $this->bank;
    }

    public function getPitch(): int
    {
        return $this->pitch;
    }

    public function isLandingLightsOn(): bool
    {
        return $this->isLandingLightsOn;
    }

    public function getTrack(): FlightTrack
    {
        return $this->track;
    }

    public function setTrack(FlightTrack $track): void
    {
        $this->track = $track;
    }

    public function getParkingBrake(): int
    {
        return $this->parkingBrake;
    }
}
