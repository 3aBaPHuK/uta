<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class FlightTrack
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\OneToMany(mappedBy: 'track', targetEntity: FlightTrackItem::class)]
    private ?Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function created(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function getItems(): ?Collection
    {
        return $this->items;
    }

    public function addTrackItem(FlightTrackItem $item): void
    {
        $item->setTrack($this);
        $this->items->add($item);
    }
}
