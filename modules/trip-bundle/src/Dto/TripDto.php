<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use App\Modules\TripBundle\Entity\Trip;

final class TripDto
{
    public readonly string $flightNo;
    public readonly string $from;
    public readonly string $to;
    public readonly string $dutyTime;
    public readonly string $status;

    public static function fromEntity(Trip $trip): self
    {
        $me = new self();

        $leg = $trip->currentLeg() ?? $trip->getFirstLeg();

        $me->flightNo = $leg->getFlightNumber();
        $me->from = $trip->from()->getIcao();
        $me->to = $trip->to()->getIcao();
        $me->dutyTime = '12h';
        $me->status = $trip->getStatus();

        return $me;
    }
}
