<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;

final class FinishLegDto
{
    public function __construct(
        public readonly string $arrivalGate,
        public readonly DateTimeImmutable $touchdownTime,
        public readonly DateTimeImmutable $arrivalTime,
        public readonly int $fuelOnBoard,
        public readonly int $touchdownFpm
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);

        return new self(
            $data['arrivalGate'],
            new DateTimeImmutable($data['touchdownTime']),
            new DateTimeImmutable($data['arrivalTime']),
            $data['fuelOnBoard'],
            $data['touchdownFpm'],
        );
    }
}
