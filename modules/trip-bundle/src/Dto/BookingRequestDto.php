<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;

final class BookingRequestDto
{
    public function __construct(
        public readonly string $aircraftType,
        public readonly ?DateTimeImmutable $etd
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(),true);

        return new self($data['aircraftType'], $data['etd'] ? new DateTimeImmutable($data['etd']) : null);
    }
}
