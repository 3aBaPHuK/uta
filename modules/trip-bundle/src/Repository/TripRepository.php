<?php

namespace App\Modules\TripBundle\Repository;

use App\Modules\TripBundle\Entity\Trip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TripRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trip::class);
    }

    public function save(Trip $trip)
    {
        $this->_em->persist($trip);
        $this->_em->flush();
    }
}
