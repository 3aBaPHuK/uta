<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use App\Modules\TripBundle\Entity\TripLeg;
use InvalidArgumentException;

final class ImpossibleToAddTripLegException extends InvalidArgumentException
{
    public static function throwFromTripLeg(TripLeg $leg): void
    {
        throw new self("Latest trip leg arrival airport should be equal with new leg departure. Arrival airport is: " . $leg->getTo()->getIcao());
    }
}
