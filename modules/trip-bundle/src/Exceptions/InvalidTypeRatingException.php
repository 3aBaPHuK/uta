<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use App\Modules\TripBundle\Entity\Trip;
use InvalidArgumentException;

final class InvalidTypeRatingException extends InvalidArgumentException
{
    public static function throwFromTrip(Trip $trip): void
    {
        self::throwFromTypeRatingIcao($trip->getAssignedAircraft()->getType()->getIcao());
    }

    public static function throwFromTypeRatingIcao(string $typeRatingIcao): void
    {
        throw new self("You have no rating for " . $typeRatingIcao);
    }
}
