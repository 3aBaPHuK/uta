<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use InvalidArgumentException;

final class TripLegsReadonlyException extends InvalidArgumentException
{
    public static final function throwException(): void
    {
        throw new self("Trip is in readonly status.");
    }
}
