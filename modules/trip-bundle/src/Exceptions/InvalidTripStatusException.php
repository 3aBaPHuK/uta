<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use App\Modules\TripBundle\Entity\Trip;
use InvalidArgumentException;

final class InvalidTripStatusException extends InvalidArgumentException
{
    public static function throwFromTrip(Trip $trip): void
    {
        throw new self("Invalid trip status: " . $trip->getStatus());
    }
}
