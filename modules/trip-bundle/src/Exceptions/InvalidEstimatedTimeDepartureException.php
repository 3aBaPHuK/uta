<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use DateTimeImmutable;
use InvalidArgumentException;

final class InvalidEstimatedTimeDepartureException extends InvalidArgumentException
{
    public static function throwFromEtd(DateTimeImmutable $etd): void
    {
        throw new self("Impossible to set departure time in past. ETD: ".$etd->format('H:i:s'));
    }
}
