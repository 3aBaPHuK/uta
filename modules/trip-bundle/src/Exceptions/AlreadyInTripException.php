<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use InvalidArgumentException;

final class AlreadyInTripException extends InvalidArgumentException
{
    public static final function throwException(): void
    {
        throw new self("You're already in trip.");
    }
}
