<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\PilotBundle\Entity\Pilot;
use InvalidArgumentException;

final class InvalidPilotLocationException extends InvalidArgumentException
{
    public static function throwFromPilotAndLocation(Pilot $pilot, Airport $location)
    {
        throw new self("You currently located in " . $pilot->getLocation()->getIcao() . ". So you can't join trip from " . $location->getIcao());
    }
}
