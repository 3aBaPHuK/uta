<?php

namespace App\Modules\TripBundle\Controller;

use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TimetableBundle\Interfaces\TimetableServiceInterface;
use App\Modules\TripBundle\Dto\BookingRequestDto;
use App\Modules\TripBundle\Dto\FinishLegDto;
use App\Modules\TripBundle\Dto\TripDto;
use App\Modules\TripBundle\Dto\TripRequestDto;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripStarterInterface;
use App\Modules\TripBundle\TripAction\AddLegAction;
use App\Modules\TripBundle\TripAction\BookTripAction;
use App\Modules\TripBundle\TripAction\FinishLegAction;
use App\Modules\TripBundle\TripAction\JoinTripAction;
use App\Modules\TripBundle\TripAction\StartNextLegAction;
use App\Modules\TripBundle\TripAction\StartTripAction;
use App\Modules\TripBundle\Value\AddLegRequest;
use App\Modules\TripBundle\Value\BookTripRequest;
use App\Modules\TripBundle\Value\FinishLegRequest;
use App\Modules\TripBundle\Value\JoinTripRequest;
use App\Modules\TripBundle\Value\StartNextLegRequest;
use App\Modules\TripBundle\Value\StartTripRequest;
use DateTimeImmutable;
use InvalidArgumentException;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;

class TripController extends AbstractController
{
    public function __construct(private readonly TripStarterInterface $starter)
    {
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=TripRequestDto::class)))
     */
    #[Route(path: '/api/trip/list/available', name: 'available_trip_list', methods: ['POST'])]
    #[Tag('Trip')]
    #[IsGranted('ROLE_PILOT')]
    public function getAvailableTripList(Request $request, TimetableServiceInterface $timetableService): JsonResponse
    {
        $timetableDto = TripRequestDto::fromRequest($request);

        return new JsonResponse($timetableService->getActualSchedule(
            $timetableDto->getDateOfFlight() ?? new DateTimeImmutable('today'),
            $timetableDto->getFrom(),
            $timetableDto->getTo(),
            $timetableDto->getAircraftType(),
            (int)$request->get('page', 1),
            (int)$request->get('page_size', 15)
        ));
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=BookingRequestDto::class)))
     */
    #[Tag('Trip')]
    #[Route(path: '/api/trip/book/{flight}', name: 'book_trip_from_flight', methods: ['POST'])]
    #[ParamConverter('flight', class: Flight::class)]
    #[IsGranted('ROLE_PILOT')]
    public function doBooking(BookTripRequest $tripRequest, BookTripAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('Trip')]
    #[Route(path: '/api/trip/{trip}/join', name: 'join_trip', methods: ['POST'])]
    #[ParamConverter('trip', class: Trip::class)]
    #[IsGranted('ROLE_PILOT')]
    public function joinToBookedTrip(JoinTripRequest $tripRequest, JoinTripAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('Trip')]
    #[Route(path: '/api/trip/{trip}/add-leg/{flight}', name: 'trip_add_leg', methods: ['POST'])]
    #[ParamConverter('trip', class: Trip::class)]
    #[ParamConverter('flight', class: Flight::class)]
    #[IsGranted('ROLE_PILOT')]
    public function addTripLeg(AddLegRequest $tripRequest, AddLegAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('Trip')]
    #[Route(path: '/api/trip/{trip}/start', name: 'start_trip', methods: ['POST'])]
    #[ParamConverter('trip', class: Trip::class)]
    #[IsGranted('ROLE_PILOT')]
    public function startTrip(StartTripRequest $tripRequest, StartTripAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=FinishLegDto::class)))
     */
    #[Tag('Trip')]
    #[Route(path: '/api/trip/{trip}/next-leg', name: 'start_next_leg', methods: ['POST'])]
    #[ParamConverter('trip', class: Trip::class)]
    #[IsGranted('ROLE_PILOT')]
    public function startNextTripLeg(StartNextLegRequest $tripRequest, StartNextLegAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=FinishLegDto::class)))
     */
    #[Tag('Trip')]
    #[Route(path: '/api/trip/{trip}/finish-last-leg', name: 'finish_last_leg', methods: ['POST'])]
    #[ParamConverter('trip', class: Trip::class)]
    #[IsGranted('ROLE_PILOT')]
    public function finishLastLeg(FinishLegRequest $tripRequest, FinishLegAction $action): JsonResponse
    {
        try {
            return new JsonResponse(TripDto::fromEntity($this->starter->startAction($action, $tripRequest)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }
}
