<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Repository\TripRepository;

class AddLegRequest extends TripRequest
{
    public function __construct(
        Pilot $pilot, Trip $trip, TripRepository $repository, private readonly Flight $flight
    )
    {
        parent::__construct($pilot, $repository, $trip);
    }

    public function flight(): Flight
    {
        return $this->flight;
    }
}
