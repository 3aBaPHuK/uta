<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Exceptions\AlreadyInTripException;
use App\Modules\TripBundle\Repository\TripRepository;

abstract class TripRequest
{
    public function __construct(
        private readonly Pilot $pilot,
        private readonly TripRepository $repository,
        private readonly ?Trip $trip = null
    )
    {
    }

    public function pilot(): Pilot
    {
        return $this->pilot;
    }

    public function trip(): ?Trip
    {
        return $this->trip;
    }

    public function repository(): TripRepository
    {
        return $this->repository;
    }

    protected function assertPilotAvailable(Pilot $pilot): void
    {
        if ($this->repository->findBy(['captain' => $pilot, 'status' => [Trip::STATUS_BOOKED, Trip::STATUS_PROGRESS]]) || $this->repository->findBy(['firstOfficer' => $pilot, 'status' => [Trip::STATUS_BOOKED, Trip::STATUS_PROGRESS]])) {
            AlreadyInTripException::throwException();
        }
    }
}
