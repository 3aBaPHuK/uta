<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TripBundle\Dto\BookingRequestDto;
use App\Modules\TripBundle\Repository\TripRepository;
use DateTimeImmutable;

class BookTripRequest extends TripRequest
{
    public function __construct(
        Pilot $pilot,
        TripRepository $repository,
        private readonly Flight $flight,
        private readonly BookingRequestDto $bookingRequestDto
    )
    {
        parent::__construct($pilot, $repository);

        $this->assertPilotAvailable($pilot);
    }

    public function flight(): Flight
    {
        return $this->flight;
    }

    public function aircraftTypeIcao(): string
    {
        return $this->bookingRequestDto->aircraftType;
    }

    public function etd(): DateTimeImmutable
    {
        return $this->bookingRequestDto->etd;
    }
}
