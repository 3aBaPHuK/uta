<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Service;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Event\AfterTripActionExecutedEvent;
use App\Modules\TripBundle\Event\BeforeTripActionEvent;
use App\Modules\TripBundle\Event\TripFinishedEvent;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Interfaces\TripStarterInterface;
use App\Modules\TripBundle\Value\TripRequest;
use Psr\EventDispatcher\EventDispatcherInterface;

class TripStarterService implements TripStarterInterface
{
    public function __construct(private readonly EventDispatcherInterface $eventDispatcher)
    {
    }

    public function startAction(TripActionInterface $action, TripRequest $tripRequest): Trip
    {
        $this->eventDispatcher->dispatch(new BeforeTripActionEvent($tripRequest->pilot(), $tripRequest->trip()));
        $tripAircraft = $tripRequest->trip()?->getAssignedAircraft();

        $trip = $action($tripRequest);
        $this->eventDispatcher->dispatch(new AfterTripActionExecutedEvent($tripRequest->pilot(), $trip));

        if ($trip->isInStatus(Trip::STATUS_FINISHED))
        {
            $this->eventDispatcher->dispatch(new TripFinishedEvent($trip, $tripAircraft));
        }

        return $trip;
    }
}
