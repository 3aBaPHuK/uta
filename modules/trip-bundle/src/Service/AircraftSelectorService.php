<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Service;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AircraftBundle\Repository\AircraftRepository;
use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\TripBundle\Interfaces\AircraftSelectorInterface;

class AircraftSelectorService implements AircraftSelectorInterface
{
    public function __construct(
        private readonly AircraftRepository $aircraftRepository,
        private readonly AircraftTypeRepository $aircraftTypeRepository,
    )
    {
    }

    public function getAvailableAircraftByTypeAndLocation(string $aircraftTypeIcao, Airport $location): ?Aircraft
    {
        return $this->aircraftRepository
            ->findByAircraftType($this->aircraftTypeRepository->findByIcao($aircraftTypeIcao))
            ->getFirstAvailableInLocation($location);
    }
}
