<?php

declare(strict_types=1);

namespace App\Modules\AviationEdgeParserBundle\Command;

use App\Modules\AviationEdgeParserBundle\Event\AviationEdgeRouteImportFinishedEvent;
use App\Modules\AviationEdgeParserBundle\Interface\AviationEdgeRoutesServiceInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ParseRoutesCommand extends Command
{
    public function __construct(
        private AviationEdgeRoutesServiceInterface $routesService,
        private EventDispatcherInterface $eventDispatcher
    )
    {
        parent::__construct('aviation-edge:import-routes');
    }

    protected function configure()
    {
        $this->setDescription('Import Aviation Edge routes for company');
        $this->setHelp('Example: bin/console aviation-edge:import-routes --icao="SBI"');

        $this->addOption(
            'icao',
            null,
            InputOption::VALUE_REQUIRED,
            'for example: SBI'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $scheduleItems = $this->routesService->getRoutes($input->getOption('icao'));

        $this->eventDispatcher->dispatch(new AviationEdgeRouteImportFinishedEvent($scheduleItems));

        return 0;
    }
}
