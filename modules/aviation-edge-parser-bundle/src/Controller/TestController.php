<?php

namespace App\Modules\AviationEdgeParserBundle\Controller;

use App\Modules\AviationEdgeParserBundle\Event\AviationEdgeRouteImportFinishedEvent;
use App\Modules\AviationEdgeParserBundle\Interface\AviationEdgeRoutesServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    public function __construct(private AviationEdgeRoutesServiceInterface $routesService, private EventDispatcherInterface $eventDispatcher)
    {
    }
    #[Route(path: 'admin/test/aviation-edge', name: 'aviation_edge_test', methods: ['GET'])]
    public function blaAction(): Response
    {
        $routes = $this->routesService->getRoutes('UTA');
        $this->eventDispatcher->dispatch(new AviationEdgeRouteImportFinishedEvent($routes));

        return new Response();
    }
}
