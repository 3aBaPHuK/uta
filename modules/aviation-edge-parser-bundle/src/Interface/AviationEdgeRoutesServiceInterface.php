<?php

namespace App\Modules\AviationEdgeParserBundle\Interface;

interface AviationEdgeRoutesServiceInterface
{
    /**
     * @return AviationEdgeRouteInterface[]
     */
    public function getRoutes(string $airlineIcao): array;
}
