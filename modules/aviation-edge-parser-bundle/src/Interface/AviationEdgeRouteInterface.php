<?php

namespace App\Modules\AviationEdgeParserBundle\Interface;

interface AviationEdgeRouteInterface
{
    public function airlineIcao(): string;

    public function arrivalIcao(): string;

    public function departureIcao(): string;

    public function departureTime(): ?string;

    public function flightNumber(): string;
}
