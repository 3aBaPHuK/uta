<?php

declare(strict_types=1);

namespace App\Modules\AviationEdgeParserBundle\Service;

use App\Modules\AviationEdgeParserBundle\Interface\AviationEdgeRoutesServiceInterface;
use App\Modules\AviationEdgeParserBundle\Transport\AviationEdgeClient;
use App\Modules\AviationEdgeParserBundle\Value\AviationEdgeRoute;
use GuzzleHttp\RequestOptions;

class AviationEdgeRoutesService implements AviationEdgeRoutesServiceInterface
{
    public function __construct(
        private AviationEdgeClient $client
    )
    {
    }

    public function getRoutes(string $airlineIcao): array
    {
        $response = $this->client->get('routes', [
            RequestOptions::QUERY => array_merge(
                ['airlineIcao' => $airlineIcao],
                $this->client->getConfig()['query']
            )
        ]);

        $responseArray = json_decode((string) $response->getBody(), true);

        return array_map(function (array $route) {
            return new AviationEdgeRoute(
                $route['airlineIcao'],
                $route['arrivalIcao'],
                $route['departureIcao'],
                $route['departureTime'],
                $route['flightNumber']
            );
        },  $responseArray);
    }
}
