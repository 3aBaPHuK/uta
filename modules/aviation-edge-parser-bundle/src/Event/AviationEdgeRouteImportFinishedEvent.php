<?php

declare(strict_types=1);

namespace App\Modules\AviationEdgeParserBundle\Event;

use App\Modules\AviationEdgeParserBundle\Interface\AviationEdgeRouteInterface;

class AviationEdgeRouteImportFinishedEvent
{

    public function __construct(private array $routesList)
    {
    }

    /**
     * @return AviationEdgeRouteInterface[]
     */
    public function getRoutesList(): array
    {
        return $this->routesList;
    }
}
