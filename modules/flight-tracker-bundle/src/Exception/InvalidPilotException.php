<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Exception;

use App\Modules\PilotBundle\Entity\Pilot;
use InvalidArgumentException;

class InvalidPilotException extends InvalidArgumentException
{
    public static function wrongPilotStatus(Pilot $pilot): self
    {
        return new self("Wring pilot status");
    }

    public static function unknownPilot(): self
    {
        return new self("Unknown pilot");
    }
}
