<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Service;

use App\Modules\FlightTrackerBundle\Dto\FlightStepsVector;
use App\Modules\FlightTrackerBundle\Interfaces\FlightTrackerWriterServiceInterface;
use App\Modules\FlightTrackerBundle\Repository\FlightTrackRepository;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;

class FlightTrackerWriterService implements FlightTrackerWriterServiceInterface
{
    public function __construct(private FlightTrackRepository $flightTrackRepository)
    {
    }

    public function writeTrack(Pilot $pilot, Flight $flight, FlightStepsVector $flightStepsVector): void
    {

    }
}
