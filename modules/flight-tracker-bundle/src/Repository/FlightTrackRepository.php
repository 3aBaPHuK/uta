<?php

namespace App\Modules\FlightTrackerBundle\Repository;

use App\Modules\FlightTrackerBundle\Entity\FlightTrack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FlightTrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FlightTrack::class);
    }
}
