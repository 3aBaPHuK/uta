<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Dto;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;
use IteratorAggregate;
use Traversable;

final class FlightStepsVector implements IteratorAggregate
{
    private array $flightSteps;

    public function __construct(FlightStep ...$flightSteps)
    {
        $this->flightSteps = $flightSteps;
    }

    public function add(FlightStep $step): self
    {
        $this->flightSteps[] = $step;

        return $this;
    }

    public static function fromJson(Pilot $pilot, Flight $flight, string $json): self
    {
        $data = json_decode($json, true);

        return new self(...array_map(function (array $item) use ($pilot, $flight) {
            return FlightStep::fromMessageArray($flight->getId(), $flight->getFlightNumber(), $item);
        },  $data));
    }

    public function getIterator(): Traversable
    {
        usort($this->flightSteps, function (FlightStep $a, FlightStep $b) {
            return $a < $b ? -1 : 1;
        });

        yield from $this->flightSteps;
    }
}
