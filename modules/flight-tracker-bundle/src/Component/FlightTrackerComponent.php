<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Component;

use App\Modules\FlightTrackerBundle\Dto\FlightStep;
use App\Modules\FlightTrackerBundle\Dto\FlightStepsVector;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;

class FlightTrackerComponent implements MessageComponentInterface
{

    private SplObjectStorage $connections;

    /**
     * @var FlightStepsVector[]
     */
    private array $vectors = [];

    public function __construct()
    {
        $this->connections = new SplObjectStorage();
    }

    function onOpen(ConnectionInterface $conn)
    {
        $this->connections->attach($conn);
        $conn->send(json_encode([
            'message' => 'Welcome. Please login'
        ]));
        echo "New connection \n";
    }

    function onClose(ConnectionInterface $conn)
    {
        $this->connections->detach($conn);
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->connections->detach($conn);
        $conn->close();
    }

    function onMessage(ConnectionInterface $from, $msg)
    {
        var_dump($this->connections);
        echo "new Message: ".$msg."\n";
        $flightStep = FlightStep::fromMessageString($msg);

        if (empty($this->vectors[$flightStep->login])) {
            $this->vectors[$flightStep->login] = new FlightStepsVector($flightStep);
        } else {
            $this->vectors[$flightStep->login]->add($flightStep);
        }

        var_dump($this->vectors[$flightStep->login]);
    }
}
