<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Command;

use App\Modules\FlightTrackerBundle\Component\FlightTrackerComponent;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunFlightTrackerCommand extends Command
{
    public function __construct()
    {
        parent::__construct('flight-tracker:run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $port = 3001;
        $output->writeln("Starting server on port " . $port);
        $server = IoServer::factory(new HttpServer(new WsServer(new FlightTrackerComponent())), $port);
        $server->run();
        return 0;
    }
}
