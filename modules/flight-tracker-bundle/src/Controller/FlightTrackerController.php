<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Controller;

use App\Modules\FlightTrackerBundle\Dto\FlightStepsVector;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FlightTrackerController extends AbstractController
{
    #[Route(path: '/api/{pilot}/flight/{flight}/receive-track', name: 'flight_tracker_receive_track', methods: ['POST'])]
    #[ParamConverter('pilot', class: Pilot::class)]
    #[ParamConverter('flight', class: Flight::class)]
    #[IsGranted('ROLE_PILOT')]
    public function receiveFullTrack(Pilot $pilot, Flight $flight, Request $request): JsonResponse
    {
        $track = FlightStepsVector::fromJson($pilot, $flight, $request->getContent());

        return new JsonResponse();
    }
}
