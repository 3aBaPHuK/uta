<?php

namespace App\Modules\PilotBundle\Interfaces;

use App\Modules\PilotBundle\Entity\Pilot;

interface CurrentPilotInterface
{
    public function getCurrentPilot(): Pilot;
}
