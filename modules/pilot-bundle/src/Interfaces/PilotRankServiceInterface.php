<?php

namespace App\Modules\PilotBundle\Interfaces;

use App\Modules\PilotBundle\Entity\Minimum;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\Rank;

interface PilotRankServiceInterface
{
    public function updatePilotRank(Pilot $pilot, Rank $rank): void;

    public function updatePilotMinimum(Pilot $pilot, Minimum $minimum): void;

}
