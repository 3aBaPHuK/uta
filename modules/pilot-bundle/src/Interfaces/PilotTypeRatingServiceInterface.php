<?php

namespace App\Modules\PilotBundle\Interfaces;

use App\Modules\PilotBundle\Entity\Pilot;

interface PilotTypeRatingServiceInterface
{
    public function grantRating(Pilot $pilot, string $rating): void;

    public function removeRating(Pilot $pilot, string $rating): void;
}
