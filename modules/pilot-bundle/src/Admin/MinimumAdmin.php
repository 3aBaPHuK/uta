<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MinimumAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('value', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('id')->add('value');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('id')->add('value');
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('id')->add('value');
    }
}
