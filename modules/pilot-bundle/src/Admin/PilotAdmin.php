<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class PilotAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('pid', TextType::class)
            ->add('email', EmailType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('pid');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('pid');
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('pid');
    }
}
