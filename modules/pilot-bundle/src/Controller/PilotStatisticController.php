<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Controller;

use App\Modules\PilotBundle\Dto\PilotStatisticDto;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PilotStatisticController extends AbstractController
{

    /**
     * @OA\Response(response=200, description="Pilot logbook")
     * @OA\Response(response=401, description="Unauthorized")
     */
    #[Route(path: '/api/pilot/logbook', name: 'logbook', methods: ["GET"])]
    #[IsGranted("ROLE_PILOT")]
    #[Tag('Pilot')]
    public function currentUserStatistic(): JsonResponse
    {
        /** TODO: Pilot repo instead of getUser() */
        return new JsonResponse(PilotStatisticDto::fromPilotEntity($this->getUser()));
    }

    /**
     * @OA\RequestBody()
     */
    #[Route(path: '/api/pilot/logbook', name: 'create_report', methods: ["POST"])]
    #[IsGranted("ROLE_PILOT")]
    #[Tag('Pilot')]
    public function createReport(Request $request): JsonResponse
    {
        //TODO: Implements when dependencies will be ready

        return new JsonResponse(['message' => 'Not implemented yet'], Response::HTTP_TOO_EARLY);
    }
}
