<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Controller;

use App\Modules\PilotBundle\Dto\MinimumDto;
use App\Modules\PilotBundle\Dto\PilotStatisticDto;
use App\Modules\PilotBundle\Dto\RankDto;
use App\Modules\PilotBundle\Entity\Minimum;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\Rank;
use App\Modules\PilotBundle\Interfaces\PilotRankServiceInterface;
use App\Modules\PilotBundle\Interfaces\PilotTypeRatingServiceInterface;
use App\Modules\PilotBundle\Repository\MinimumRepository;
use App\Modules\PilotBundle\Repository\PilotRepository;
use App\Modules\PilotBundle\Repository\RankRepository;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;


class PilotAdminController extends AbstractController
{
    public function __construct(
        private PilotRankServiceInterface $pilotRankService,
        private PilotTypeRatingServiceInterface $pilotTypeRatingService,
    )
    {
    }

    #[Route(path: '/api/rank/list', name: 'rank_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function rankList(RankRepository $rankRepository): JsonResponse
    {
        return $this->json(array_map(function (Rank $rank) {
            return RankDto::fromEntity($rank);
        }, $rankRepository->findAll()));
    }

    #[Route(path: '/api/minimum/list', name: 'minimum_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function minimumList(MinimumRepository $minimumRepository): JsonResponse
    {
        return $this->json(array_map(function (Minimum $minimum) {
            return MinimumDto::fromEntity($minimum);
        }, $minimumRepository->findAll()));
    }

    /**
     * @OA\Response(response=204, description="Done")
     * @OA\Response(response=401, description="Unauthorized")
     * @OA\Response(response=403,description="Forbidden")
     */
    #[Route(path: '/api/pilot/{pilot}/rank/{rank}', name: 'rank_update', methods: ["PUT"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[ParamConverter('rank', class: 'App\Modules\PilotBundle\Entity\Rank')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function updatePilotRank(Pilot $pilot, Rank $rank): JsonResponse
    {
        $this->pilotRankService->updatePilotRank($pilot, $rank);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/api/pilot/{pilot}/type-rating/{type}', name: 'grant_type_rating', methods: ["PUT"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function grantTypeRating(Pilot $pilot, string $type): JsonResponse
    {
        $this->pilotTypeRatingService->grantRating($pilot, $type);

        return new JsonResponse(PilotStatisticDto::fromPilotEntity($pilot), Response::HTTP_OK);
    }

    #[Route(path: '/api/pilot/{pilot}/type-rating/{type}', name: 'remove_type_rating', methods: ["DELETE"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function removeTypeRating(Pilot $pilot, string $type): JsonResponse
    {
        $this->pilotTypeRatingService->removeRating($pilot, $type);

        return new JsonResponse(PilotStatisticDto::fromPilotEntity($pilot), Response::HTTP_OK);
    }

    /**
     * @OA\Response(response=204,description="Done")
     * @OA\Response(response=401,description="Unauthorized")
     * @OA\Response(response=403,description="Forbidden")
     */
    #[Route(path: '/api/pilot/{pilot}/minimum/{minimum}', name: 'minimum_update', methods: ["PUT"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[ParamConverter('minimum', class: 'App\Modules\PilotBundle\Entity\Minimum')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function setPilotMinimum(Pilot $pilot, Minimum $minimum): JsonResponse
    {
        $this->pilotRankService->updatePilotMinimum($pilot, $minimum);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @OA\Response(response=200, description="Pilots list")
     * @OA\Response(response=401, description="Unauthorized")
     * @OA\Response(response=403, description="Forbidden")
     */
    #[Route(path: '/api/pilots/list', name: 'pilots_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[IsGranted("ROLE_ADMIN")]
    public function getPilotsList(Request $request, PilotRepository $pilotRepository): JsonResponse
    {
        return new JsonResponse(
            $pilotRepository->gitPilotsList(
                (int) $request->get('page', 1),
                (int) $request->get('page_size', 15)
            )
        );
    }
}
