<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Dto;

use App\Modules\PilotBundle\Entity\Rank;

final class RankDto
{

    private function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly string $icon
    )
    {
    }

    public final static function fromEntity(Rank $rank): self
    {
        return new self(
            $rank->getId(),
            $rank->getName(),
            $rank->getIcon()
        );
    }
}
