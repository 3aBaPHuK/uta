<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class PilotsListDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, PilotStatisticDto ...$pilots)
    {
        $this->items = $pilots;
        $this->totalResults = $totalResults;
    }

    public function current(): PilotStatisticDto
    {
        return current($this->items);
    }
}
