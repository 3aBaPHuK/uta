<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Dto;

use App\Modules\PilotBundle\Entity\LogbookItem;

final class LogbookItemDto
{
    private function __construct(
        public string $created,
        public string $from,
        public string $to,
        public string $flightTime,
        public string $landingAirport,
        public string $route,
    )
    {
    }

    public static final function fromEntity(LogbookItem $item): self
    {
        return new self(
            $item->created()->format('Y-m-d H:i:s'),
            $item->departure(),
            $item->arrival(),
            date('H:i', mktime(0, $item->flightTime())),
            $item->landingAirport(),
            $item->route()
        );
    }
}
