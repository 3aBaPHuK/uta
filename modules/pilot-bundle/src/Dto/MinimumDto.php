<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Dto;

use App\Modules\PilotBundle\Entity\Minimum;

final class MinimumDto
{

    private function __construct(
        public int $id,
        public string $value,
    )
    {
    }

    public final static function fromEntity(Minimum $minimum): self
    {
        return new self(
            $minimum->getId(),
            $minimum->getValue(),
        );
    }
}
