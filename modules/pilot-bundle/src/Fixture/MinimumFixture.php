<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Fixture;

use App\Modules\PilotBundle\Entity\Minimum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MinimumFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $minimum1 = new Minimum();
        $minimum1->setValue("80x1000x400");

        $manager->persist($minimum1);

        $minimum2 = new Minimum();
        $minimum2->setValue("60x800x300");

        $manager->persist($minimum2);

        $minimum3 = new Minimum();
        $minimum3->setValue("30x350x200");

        $manager->persist($minimum3);

        $manager->flush();
    }
}
