<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Fixture;

use App\Modules\AirportBundle\Interfaces\BaseAirportServiceInterface;
use App\Modules\ConfigurationBundle\Fixtures\InitialConfigurationFixture;
use App\Modules\PilotBundle\Entity\Pilot;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class PilotFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(private readonly PasswordHasherInterface $hasher, private readonly BaseAirportServiceInterface $baseAirportService)
    {
    }

    public function load(ObjectManager $manager)
    {
        $baseAirport = $this->baseAirportService->getBaseAirport();

        $manager->persist(
            new Pilot(
                '111111',
                'pilot1@test.com',
                'John Doe',
                new DateTimeImmutable('-14 years'),
                $this->hasher->hash('123456'),
                $baseAirport
            )
        );

        $manager->persist(
            new Pilot(
                '222222',
                'pilot2@test.com',
                'Bill Gates',
                new DateTimeImmutable('-58 years'),
                $this->hasher->hash('123456'),
                $baseAirport
            )
        );

        $manager->persist(
            new Pilot(
                '333333',
                'pilot3@test.com',
                'Gary Moore',
                new DateTimeImmutable('-40 years'),
                $this->hasher->hash('123456'),
                $baseAirport
            )
        );

        $manager->persist(
            new Pilot(
                '444444',
                'pilot4@test.com',
                'Bruce Dickinson',
                new DateTimeImmutable('-18 years'),
                $this->hasher->hash('123456'),
                $baseAirport
            )
        );

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [InitialConfigurationFixture::class];
    }
}
