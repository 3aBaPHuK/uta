<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\EventSubscriber;

use App\Modules\PilotBundle\Entity\LogbookItem;
use App\Modules\PilotBundle\Repository\PilotRepository;
use App\Modules\TripBundle\Event\TripFinishedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TripFinishedEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly PilotRepository $repository)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [TripFinishedEvent::class => 'onTripFinished'];
    }

    public function onTripFinished(TripFinishedEvent $event)
    {
        $tripDepartureTime = $event->trip()->getFirstLeg()->departureDateTime();
        $tripArrivalTime = $event->trip()->getLastLeg()->arrived();
        $tripFrom = $event->trip()->from()->getIcao();
        $tripTo = $event->trip()->to()->getIcao();
        $tripMinutes = $tripArrivalTime->diff($tripDepartureTime)->i;
        $tripRoute = $event->trip()->from()->getIcao() . '-' . $event->trip()->to()->getIcao();

        $event->trip()->captain()->addLogbookItem(
            new LogbookItem($tripFrom, $tripTo, $tripMinutes, $tripTo, $tripRoute, $event->trip()->captain())
        );
        $this->repository->save($event->trip()->captain());

        if (!$event->trip()->firstOfficer()) {
            return;
        }

        $event->trip()->firstOfficer()->addLogbookItem(
            new LogbookItem($tripFrom, $tripTo, $tripMinutes, $tripTo, $tripRoute, $event->trip()->firstOfficer())
        );

        $this->repository->save($event->trip()->firstOfficer());
    }
}
