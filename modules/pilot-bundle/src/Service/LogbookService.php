<?php

namespace App\Modules\PilotBundle\Service;

use App\Modules\PilotBundle\Entity\LogbookItem;
use App\Modules\PilotBundle\Interfaces\LogbookServiceInterface;
use App\Modules\PilotBundle\Repository\LogbookRepository;
use DateTimeImmutable;
use Symfony\Component\Security\Core\Security;

class LogbookService implements LogbookServiceInterface
{
    public function __construct(private LogbookRepository $logbookRepository, private Security $security)
    {
    }

    public function createReport(string $from, string $to, string $landingAirport, DateTimeImmutable $departure, DateTimeImmutable $arrival, string $route, ?string $delayReason = null): void
    {
        $flightTime = abs($departure->getTimestamp() - $arrival->getTimestamp()) / 60;

        $logbookItem = new LogbookItem(
            new DateTimeImmutable(),
            $from,
            $to,
            $flightTime,
            $landingAirport,
            $route,
            $this->security->getUser()
        );

        $this->logbookRepository->save($logbookItem);
    }
}
