<?php

namespace App\Modules\PilotBundle\Service;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Interfaces\CurrentPilotInterface;
use App\Modules\PilotBundle\Repository\PilotRepository;
use App\Modules\UserBundle\Entity\User;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

class CurrentPilotService implements CurrentPilotInterface
{
    public function __construct(private readonly Security $security, private readonly PilotRepository $repository)
    {
    }

    public function getCurrentPilot(): Pilot
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();
        if (!$currentUser) {
            throw new UnauthorizedHttpException('Unathorized', 'Unathorized');
        }

        if (!$pilot = $this->repository->getById($currentUser->getId())) {
            throw new AccessDeniedException('Forbidden', 403);
        }

        return $pilot;
    }
}
