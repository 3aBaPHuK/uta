<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Entity\Minimum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MinimumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Minimum::class);
    }
}
