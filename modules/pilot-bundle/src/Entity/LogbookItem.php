<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Entity;

use App\Modules\PilotBundle\Repository\LogbookRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LogbookRepository::class)]
class LogbookItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $created;

    #[ORM\Column(type: 'string', length: 4)]
    private string $departure;

    #[ORM\Column(type: 'string', length: 4)]
    private string $arrival;

    #[ORM\Column(type: 'integer')]
    private int $flightTime;

    #[ORM\Column(type: 'string', length: 4)]
    private string $landingAirport;

    #[ORM\Column(type: 'text')]
    private string $route;

    #[ORM\ManyToOne(targetEntity: Pilot::class, cascade: ['remove', 'persist'])]
    private Pilot $pilot;

    public function __construct(
        string $departure,
        string $arrival,
        int $flightTime,
        string $landingAirport,
        string $route,
        Pilot $pilot
    )
    {
        $this->created = new DateTimeImmutable();
        $this->departure = $departure;
        $this->arrival = $arrival;
        $this->flightTime = $flightTime;
        $this->landingAirport = $landingAirport;
        $this->route = $route;
        $this->pilot = $pilot;
    }

    public function created(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function departure(): string
    {
        return $this->{__FUNCTION__};
    }

    public function arrival(): string
    {
        return $this->{__FUNCTION__};
    }

    public function flightTime(): int
    {
        return $this->{__FUNCTION__};
    }

    public function landingAirport(): string
    {
        return $this->{__FUNCTION__};
    }

    public function route(): string
    {
        return $this->{__FUNCTION__};
    }
}
