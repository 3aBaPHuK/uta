<?php

namespace App\Modules\PilotBundle\Entity;

use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\UserBundle\Entity\User;
use ArrayIterator;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity]
class Pilot extends User
{

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $flightTime;

    #[ORM\ManyToOne(targetEntity: PilotRank::class, cascade: ['persist'], inversedBy: 'pilot')]
    private ?PilotRank $rank;

    #[ORM\ManyToOne(targetEntity: PilotMinimum::class, cascade: ['persist'], inversedBy: 'pilot')]
    private ?PilotMinimum $minimum;

    #[ORM\Column(type: 'simple_array')]
    private array $typeRatings = [];

    #[ORM\ManyToOne(targetEntity: Airport::class, cascade: ['persist'])]
    private Airport $location;

    #[ORM\OneToMany(mappedBy: 'pilot', targetEntity: LogbookItem::class, cascade: ['remove', 'persist'])]
    private ?PersistentCollection $logbookItems;

    public function __construct(
        string                $login,
        string                $email,
        string                $fullName,
        DateTimeImmutable     $birthDate,
        string                $password,
        Airport               $location,
        ?int                  $flightTime = 0,
        ?PersistentCollection $logbookItems = null
    )
    {
        $this->logbookItems = $logbookItems;
        $this->flightTime = $flightTime;
        $this->location = $location;

        parent::__construct($login, $email, $fullName, $birthDate, $password, ['ROLE_PILOT']);
    }

    public function setRank(PilotRank $rank): void
    {
        $this->rank = $rank;
    }

    public function setMinimum(PilotMinimum $minimum): void
    {
        $this->minimum = $minimum;
    }

    public function removeTypeRating(string $type): void
    {
        $this->typeRatings = array_filter($this->typeRatings, function (string $typeRating) use ($type) {
            return $typeRating !== $type;
        });
    }

    public function grantTypeRating(string $type): void
    {
        $this->typeRatings[] = $type;
        $this->typeRatings = array_unique($this->typeRatings);
    }

    public function hasRating(string $rating): bool
    {
        return in_array($rating, $this->typeRatings);
    }

    public function getTypeRatings(): array
    {
        return $this->typeRatings;
    }

    public function flightTime(): ?int
    {
        return $this->{__FUNCTION__};
    }

    public function rank(): ?PilotRank
    {
        return $this->{__FUNCTION__};
    }

    public function minimum(): ?PilotMinimum
    {
        return $this->{__FUNCTION__};
    }

    public function logbookItems(): \Traversable
    {
        return $this->logbookItems->getIterator();
    }

    public function addLogbookItem(LogbookItem $item)
    {
        $this->logbookItems->add($item);
    }

    public function getLocation(): Airport
    {
        return $this->location;
    }

    public function moveToLocation(Airport $airport): void
    {
        $this->location = $airport;
    }
}
