<?php

declare(strict_types=1);

namespace App\Modules\InfogateParserBundle\Command;

use App\Modules\InfogateParserBundle\Event\InfogateScheduleImportFinishedEvent;
use App\Modules\InfogateParserBundle\Interfaces\DomScheduleServiceInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ParseFlightsCommand extends Command
{
    private DomScheduleServiceInterface $scheduleService;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(DomScheduleServiceInterface $scheduleService, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct('infogate:import-flights');

        $this->scheduleService = $scheduleService;
        $this->eventDispatcher = $eventDispatcher;
    }

    protected function configure()
    {
        $this->setDescription('Import InfoGate schedule for company');
        $this->setHelp('Example: bin/console infogate:import-flights --icao="SBI"');

        $this->addOption(
            'icao',
            null,
            InputOption::VALUE_REQUIRED,
            'for example: SBI'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $scheduleItemsCollection = $this->scheduleService->findSchedule($input->getOption('icao'));

        $this->eventDispatcher->dispatch(new InfogateScheduleImportFinishedEvent($scheduleItemsCollection));

        return 0;
    }
}
