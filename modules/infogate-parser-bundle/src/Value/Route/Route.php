<?php

namespace App\Modules\InfogateParserBundle\Value\Route;

use App\Modules\InfogateParserBundle\Value\Airport\Airport;

final class Route
{
    public function __construct(
        private Airport $from,
        private Airport $to,
        private string $route,
        private float $distance
    )
    {
    }

    public function getFrom(): Airport
    {
        return $this->from;
    }

    public function getTo(): Airport
    {
        return $this->to;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function getDistance(): float
    {
        return $this->distance;
    }
}
