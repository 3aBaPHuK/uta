<?php

namespace App\Modules\InfogateParserBundle\Value\Route;

use Generator;
use IteratorAggregate;

final class RouteCollection implements IteratorAggregate
{
    private array $routes;

    public function __construct(Route ...$routes)
    {
        $this->routes = $routes;
    }

    public function add(Route $route) {
        $this->routes[] = $route;
    }

    public function getIterator(): Generator
    {
        foreach ($this->routes as $route) {
            yield $route;
        }
    }
}
