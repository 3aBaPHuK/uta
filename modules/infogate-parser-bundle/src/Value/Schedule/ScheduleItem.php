<?php

namespace App\Modules\InfogateParserBundle\Value\Schedule;

use DateTimeImmutable;

final class ScheduleItem
{
    private string $flightNumber;
    private string $from;
    private string $to;
    private string $estimatedTimeDeparture;
    private string $estimatedTimeEnroute;
    private string $aircraftType;
    private DateTimeImmutable $scheduledFrom;
    private DateTimeImmutable $scheduledUntil;
    private array $daysInWeak;

    public function __construct(
        string $flightNumber,
        string $from,
        string $to,
        string $estimatedTimeDeparture,
        string $estimatedTimeEnroute,
        string $aircraftType,
        DateTimeImmutable $scheduledFrom,
        DateTimeImmutable $scheduledUntil,
        array $daysInWeak,
    )
    {
        $this->flightNumber = $flightNumber;
        $this->from = $from;
        $this->to = $to;
        $this->estimatedTimeDeparture = $estimatedTimeDeparture;
        $this->estimatedTimeEnroute = $estimatedTimeEnroute;
        $this->aircraftType = $aircraftType;
        $this->scheduledFrom = $scheduledFrom;
        $this->scheduledUntil = $scheduledUntil;
        $this->daysInWeak = $daysInWeak;
    }

    public static function fromAssoc(array $data): self
    {
        return new self(
            $data['flightNumber'],
            $data['from'],
            $data['to'],
            $data['estimatedTimeDeparture'],
            $data['estimatedTimeEnroute'],
            $data['aircraftType'],
            DateTimeImmutable::createFromFormat($data['scheduledFrom']),
            DateTimeImmutable::createFromFormat('d.m.Y', $data['scheduledUntil']),
            $data['daysInWeak'],
        );
    }

    public function flightNumber(): string
    {
        return $this->{__FUNCTION__};
    }

    public function from(): string
    {
        return $this->{__FUNCTION__};
    }

    public function to(): string
    {
        return $this->{__FUNCTION__};
    }

    public function estimatedTimeDeparture(): string
    {
        return $this->{__FUNCTION__};
    }

    public function estimatedTimeEnroute(): string
    {
        return $this->{__FUNCTION__};
    }

    public function aircraftType(): string
    {
        return $this->{__FUNCTION__};
    }

    public function scheduledFrom(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function scheduledUntil(): DateTimeImmutable
    {
        return $this->{__FUNCTION__};
    }

    public function daysInWeak(): array
    {
        return $this->{__FUNCTION__};
    }
}
