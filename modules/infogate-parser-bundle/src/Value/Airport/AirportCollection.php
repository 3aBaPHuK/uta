<?php

namespace App\Modules\InfogateParserBundle\Value\Airport;

use Generator;
use IteratorAggregate;

final class AirportCollection implements IteratorAggregate
{
    private array $airports;

    public function __construct(Airport ...$airports)
    {
        foreach ($airports as $airport) {
            $this->airports[$airport->getIcaoCodeIntl()] = $airport;
        }
    }

    public function add(Airport $airport) {
        $this->airports[$airport->getIcaoCodeIntl()] = $airport;
    }

    public function getIterator(): Generator
    {
        foreach ($this->airports as $airport) {
            yield $airport;
        }
    }
}
