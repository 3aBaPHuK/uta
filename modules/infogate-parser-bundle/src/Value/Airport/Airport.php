<?php

namespace App\Modules\InfogateParserBundle\Value\Airport;

final class Airport
{
    public function __construct(
        private string $name,
        private string $nameInt,
        private string $icaoCodeRu,
        private string $icaoCodeIntl,
        private string $iataCode,
        private string $cityName,
        private int $timeZone,
        private bool $isInternational
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNameInt(): string
    {
        return $this->nameInt;
    }

    public function getIcaoCodeRu(): string
    {
        return $this->icaoCodeRu;
    }

    public function getIcaoCodeIntl(): string
    {
        return $this->icaoCodeIntl;
    }

    public function getIataCode(): string
    {
        return $this->iataCode;
    }

    public function getCityName(): string
    {
        return $this->cityName;
    }

    public function getTimeZone(): int
    {
        return $this->timeZone;
    }

    public function isInternational(): bool
    {
        return $this->isInternational;
    }
}
