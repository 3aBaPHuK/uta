<?php

namespace App\Modules\InfogateParserBundle\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class InfogateClient extends Client
{
    public function __construct(
        string $apiUrl,
        callable $defaultHandler = null,
        LoggerInterface $logger = null
    )
    {
        $stack = HandlerStack::create($defaultHandler ?: null);
        $stack->push(Middleware::log($logger ?? new NullLogger(), new MessageFormatter(MessageFormatter::DEBUG)));

        parent::__construct([
            'base_uri' => $apiUrl,
            'handler' => $stack,
            RequestOptions::HEADERS => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'
            ],
            RequestOptions::COOKIES => new CookieJar()
        ]);
    }
}
