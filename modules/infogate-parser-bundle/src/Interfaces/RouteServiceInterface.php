<?php

namespace App\Modules\InfogateParserBundle\Interfaces;

use App\Modules\InfogateParserBundle\Value\Route\RouteCollection;

interface RouteServiceInterface
{
    public function findRoute(string $from, string $to): RouteCollection;
}
