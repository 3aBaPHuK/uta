<?php

namespace App\Modules\InfogateParserBundle\Interfaces;

use App\Modules\InfogateParserBundle\Value\Schedule\ScheduleCollection;

interface DomScheduleServiceInterface
{
    public function findSchedule(string $icaoCode): ScheduleCollection;
}
