<?php

namespace App\Modules\InfogateParserBundle\Interfaces;

use App\Modules\InfogateParserBundle\Value\Airport\AirportCollection;

interface InfogateAirportServiceInterface
{
    public function getAirportList(): AirportCollection;
}
