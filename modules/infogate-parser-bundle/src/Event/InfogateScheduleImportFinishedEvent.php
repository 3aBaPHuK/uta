<?php

namespace App\Modules\InfogateParserBundle\Event;

use App\Modules\InfogateParserBundle\Value\Schedule\ScheduleCollection;

final class InfogateScheduleImportFinishedEvent
{
    public function __construct(private ScheduleCollection $scheduleCollection)
    {
    }

    public function getScheduleCollection(): ScheduleCollection
    {
        return $this->scheduleCollection;
    }
}
