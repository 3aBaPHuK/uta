<?php

namespace App\Modules\InfogateParserBundle\Service;

use App\Modules\InfogateParserBundle\Interfaces\DomScheduleServiceInterface;
use App\Modules\InfogateParserBundle\Transport\InfogateClient;
use App\Modules\InfogateParserBundle\Value\Schedule\ScheduleCollection;
use App\Modules\InfogateParserBundle\Value\Schedule\ScheduleItem;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Form;

class DomScheduleService implements DomScheduleServiceInterface
{
    public function __construct(private InfogateClient $client)
    {
    }

    public function findSchedule(string $icaoCode): ScheduleCollection
    {
        $uri = '/htme/dom_schedule.htme';
        $firstPageResponse = $this->getResponse($uri);

        $form = $this->getForm($firstPageResponse, $icaoCode);
        $searchUri = $this->getSearchUrl($uri, $form, ['SearchBtn' => 'Поиск']);
        $searchPageResult = $this->getResponse($searchUri);

        $scheduleCollection = $this->fillCollection($searchPageResult)->clearAllThereFlightNumberNotContains($icaoCode);
        $previousPageScheduleCollection = new ScheduleCollection();

        $equalResultsCount = 0;
        $pageParsedCount = 0;
        while ($equalResultsCount <= 2 && $pageParsedCount <= 150) {
            $form = $this->getForm($searchPageResult, $icaoCode);
            $searchUri = $this->getSearchUrl($uri, $form, ['EtHtmlButton2' => '1']);

            $searchPageResult = $this->getResponse($searchUri);
            sleep(1);

            $nextPageScheduleCollection = $this->fillCollection($searchPageResult);
            $pageParsedCount++;

            if ($nextPageScheduleCollection->equalsWith($previousPageScheduleCollection)) {
                $equalResultsCount++;
            } else {
                $nextPageScheduleCollection->clearAllThereFlightNumberNotContains($icaoCode);

                $scheduleCollection->mergeWith($nextPageScheduleCollection);

                if (!count($nextPageScheduleCollection)) {
                    break;
                }
            }

            $previousPageScheduleCollection = ScheduleCollection::createFromSelf($nextPageScheduleCollection);
        }

        return $scheduleCollection;
    }

    public function fillCollection(string $content): ScheduleCollection
    {
        $crawler = new Crawler();
        $crawler->addContent($content);

        $scheduleCollection = new ScheduleCollection();

        $trs = $crawler->filter('table:last-of-type tr');

        foreach ($trs as $tr) {
            $scheduleData = [];

            foreach ($tr->childNodes->getIterator() as $key => $td) {
                if ($td->nodeName !== 'td' || empty($td->textContent)) {
                    continue;
                }

                switch ($key) {
                    case 2:
                        $scheduleData['flightNumber'] = trim($td->textContent);
                        break;
                    case 4:
                        $scheduleData['from'] = trim($td->textContent);
                        break;
                    case 5:
                        $scheduleData['estimatedTimeDeparture'] = trim($td->textContent);
                        break;
                    case 6:
                        $scheduleData['estimatedTimeEnroute'] = trim($td->textContent);
                        break;
                    case 8:
                        $scheduleData['to'] = trim($td->textContent);
                        break;
                    case 9:
                        $scheduleData['aircraftType'] = trim($td->textContent);
                        break;
                    case 10:
                        $scheduleData['scheduledFrom'] = trim($td->textContent);
                        break;
                    case 11:
                        $scheduleData['scheduledUntil'] = trim($td->textContent);
                        break;
                    case 12:
                        $scheduleData['daysInWeak'] = array_filter(str_split(trim($td->textContent)), function ($day) {
                            return (bool) $day;
                        });

                        break;
                }
            }

            if (count($scheduleData) >= 9) {
                $scheduleCollection->add(ScheduleItem::fromAssoc($scheduleData));
            }
        }

        return $scheduleCollection;
    }

    public function getForm(string $content, string $icaoCode): Form
    {
        $crawler = new Crawler(null, $this->client->getConfig()['base_uri'] . '/htme/dom_schedule.htme');
        $crawler->addContent($content);

        return $crawler->filter('form[name="FIRSHtmlForm"]')->form([
            'SearchEdit' => $icaoCode
        ], 'GET');
    }

    public function getSearchUrl(string $uri, Form $form, array $additionalFormValues = []): string
    {
        $formValues = $additionalFormValues + $form->getValues();

        return $uri . '?' . http_build_query($formValues);
    }

    public function getResponse(string $uri): string|false
    {
        $firstPageResponse = (string)$this->client->get($uri)->getBody();
        return iconv("windows-1251", "utf-8", $firstPageResponse);
    }
}
