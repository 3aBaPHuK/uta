<?php

declare(strict_types=1);

namespace App\Modules\InfogateParserBundle\Service;

use App\Modules\InfogateParserBundle\Interfaces\RouteServiceInterface;
use App\Modules\InfogateParserBundle\Transport\InfogateClient;
use App\Modules\InfogateParserBundle\Value\Route\RouteCollection;

class InfogateRouteService implements RouteServiceInterface
{
    public function __construct(private InfogateClient $client)
    {
    }

    public function findRoute(string $from, string $to): RouteCollection
    {
        $routeCollection = new RouteCollection();

        $firstPageResponse = $this->client->get('/htme/route_sel.htme');
        $result = iconv("windows-1251","utf-8", (string) $firstPageResponse->getBody());

        return $routeCollection;
    }
}
