<?php

namespace App\Modules\InfogateParserBundle\Service;

use App\Modules\InfogateParserBundle\Interfaces\InfogateAirportServiceInterface;
use App\Modules\InfogateParserBundle\Transport\InfogateClient;
use App\Modules\InfogateParserBundle\Value\Airport\Airport;
use App\Modules\InfogateParserBundle\Value\Airport\AirportCollection;
use Symfony\Component\DomCrawler\Crawler;

class InfogateAirportService implements InfogateAirportServiceInterface
{
    public function __construct(private InfogateClient $client)
    {
    }

    public function getAirportList(): AirportCollection
    {
        $airportsCollection = new AirportCollection();

        $firstPageResponse = $this->client->get('/htme/airports.htme');
        $result = iconv("windows-1251","utf-8", (string) $firstPageResponse->getBody());
        $crawler = new Crawler(null, $this->client->getConfig()['base_uri'] . 'htme/airports.htme');
        $crawler->addContent($result);
        $airportsCollection = $this->fillCollection($crawler, $airportsCollection);


        for ($i = 0; $i <= 40; $i++) {
            $nextPageForm = $crawler->filter('input[name="EtHtmlButton2"]')->form(null, 'GET');
            $query = http_build_query($nextPageForm->getValues());
            $uri = '/htme/airports.htme?'.$query;

            $nextPageResponse = $this->client->get($uri);

            $result = iconv("windows-1251","utf-8", (string) $nextPageResponse->getBody());
            $crawler = new Crawler(null, $this->client->getConfig()['base_uri'] . $uri);
            $crawler->addContent($result);

            $airportsCollection = $this->fillCollection($crawler, $airportsCollection);
        }

        return $airportsCollection;
    }

    public function fillCollection(Crawler $crawler, AirportCollection $airportsCollection): AirportCollection
    {

        $trs = $crawler->filter('table[cols="1"][rows="1"][cellpadding="1"][width="100%"][height="72%"] table tr');

        foreach ($trs->getIterator() as $tr) {
            $airportData = [];

            foreach ($tr->childNodes->getIterator() as $index => $item) {
                if ($item->nodeName !== 'td') {
                    continue;
                }

                switch ($index) {
                    case 1:
                        $airportData['name'] = self::prepareNodeValue($item->nodeValue);
                    case 2:
                        $airportData['nameIntl'] = self::prepareNodeValue($item->nodeValue);
                    case 3:
                        $airportData['icaoCodeRu'] = self::prepareNodeValue($item->nodeValue);
                        break;
                    case 4:
                        $airportData['icaoCodeIntl'] = self::prepareNodeValue($item->nodeValue);
                        break;
                    case 5:
                        $airportData['iataCode'] = self::prepareNodeValue($item->nodeValue);
                        break;
                    case 8:
                        $airportData['cityName'] = self::prepareNodeValue($item->nodeValue);
                        break;
                    case 9:
                        $airportData['timeZone'] = self::prepareNodeValue($item->nodeValue);
                        break;
                    case 10:
                        $airportData['isInternational'] = self::prepareNodeValue($item->nodeValue) === 'международный';
                        break;
                }
            }

            if (empty($airportData)) {
                continue;
            }

            $airportsCollection->add(
                new Airport(
                    $airportData['name'],
                    $airportData['nameIntl'],
                    $airportData['icaoCodeRu'],
                    $airportData['icaoCodeIntl'],
                    $airportData['iataCode'],
                    $airportData['cityName'],
                    (int)$airportData['timeZone'],
                    $airportData['isInternational']
                )
            );
        }

        return $airportsCollection;
    }

    private static function prepareNodeValue(string $value): string
    {
        return trim(str_replace("&nbsp", '', $value));
    }
}
