<?php

declare(strict_types=1);

namespace App\Dto;

use GuzzleHttp\Psr7\UploadedFile;

class UploadStatusDto
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    private function __construct(public readonly string $status, public readonly string $filename)
    {
    }
    public static final function success(string $filePath): self
    {
        return new self(self::STATUS_SUCCESS, $filePath);
    }

    public static final function error(UploadedFile $file): self
    {
        return new self(self::STATUS_ERROR, $file->getClientFilename());
    }
}
