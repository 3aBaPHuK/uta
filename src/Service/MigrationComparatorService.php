<?php

namespace App\Service;

use Doctrine\Migrations\Version\Comparator;
use Doctrine\Migrations\Version\Version;

class MigrationComparatorService implements Comparator
{
    public function compare(Version $a, Version $b): int
    {
        return intval((string) $a) < intval((string) $b) ? -1 : 1;
    }
}
