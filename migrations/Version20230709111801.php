<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230709111801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flight ADD scheduled_since TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE flight ADD scheduled_till TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE flight DROP scheduled_from');
        $this->addSql('ALTER TABLE flight DROP scheduled_until');
        $this->addSql('ALTER TABLE flight DROP payload_percent');
        $this->addSql('ALTER TABLE flight DROP status');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_since IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_till IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "user" ALTER full_name SET NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER birth_date TYPE DATE');
        $this->addSql('ALTER TABLE "user" ALTER birth_date SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN "user".birth_date IS \'(DC2Type:date_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE flight ADD scheduled_from TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE flight ADD scheduled_until TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE flight ADD payload_percent INT NOT NULL');
        $this->addSql('ALTER TABLE flight ADD status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE flight DROP scheduled_since');
        $this->addSql('ALTER TABLE flight DROP scheduled_till');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_from IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_until IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "user" ALTER full_name DROP NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER birth_date TYPE DATE');
        $this->addSql('ALTER TABLE "user" ALTER birth_date DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN "user".birth_date IS NULL');
    }
}
