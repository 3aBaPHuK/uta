<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230520114221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'added type rating column for pilot';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" ADD type_ratings TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN "user".type_ratings IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER INDEX uniq_8d93d6495550c4ed RENAME TO UNIQ_8D93D649AA08CB10');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP type_ratings');
        $this->addSql('ALTER INDEX uniq_8d93d649aa08cb10 RENAME TO uniq_8d93d6495550c4ed');
    }
}
