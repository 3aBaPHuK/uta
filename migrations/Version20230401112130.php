<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
final class Version20230401112130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_dfe28e1939082b31');
        $this->addSql('CREATE INDEX IDX_DFE28E1939082B31 ON pilot_minimum (minimum_id)');
        $this->addSql('DROP INDEX uniq_9c7473dd7616678f');
        $this->addSql('CREATE INDEX IDX_9C7473DD7616678F ON pilot_rank (rank_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_DFE28E1939082B31');
        $this->addSql('CREATE UNIQUE INDEX uniq_dfe28e1939082b31 ON pilot_minimum (minimum_id)');
        $this->addSql('DROP INDEX IDX_9C7473DD7616678F');
        $this->addSql('CREATE UNIQUE INDEX uniq_9c7473dd7616678f ON pilot_rank (rank_id)');
    }
}
