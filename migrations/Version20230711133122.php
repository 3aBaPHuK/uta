<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230711133122 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE logbook_item ADD departure VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE logbook_item ADD arrival VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE logbook_item DROP "from"');
        $this->addSql('ALTER TABLE logbook_item DROP "to"');
    }
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE logbook_item ADD "from" VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE logbook_item ADD "to" VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE logbook_item DROP departure');
        $this->addSql('ALTER TABLE logbook_item DROP arrival');
    }
}
